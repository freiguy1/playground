const path = require("path");
const webpack = require("webpack");
const merge = require("webpack-merge");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HTMLWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

// to extract the css as a separate file
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

var MODE = process.env.npm_lifecycle_event === "prod" ? "production" : "development";
var filename = MODE == "production" ? "assets/js/[name]-[hash].js" : "index.js";

var common = {
    mode: MODE,
    entry: "./js/index.js",
    output: {
        path: path.join(__dirname, "../api/static"),
        // webpack -p automatically adds hash when building for production
        filename: filename,
        publicPath: '/'
    },
    plugins: [
        new HTMLWebpackPlugin({
            // Use this template to get basic responsive meta tags
            template: "index.html",
            // inject details of output file at end of body
            inject: "body"
        }),
        new webpack.DefinePlugin( {
            'AUTH0_DOMAIN': JSON.stringify(process.env.AUTH0_DOMAIN || 'ethan-playground.auth0.com'),
            'AUTH0_CLIENT_ID': JSON.stringify(process.env.AUTH0_CLIENT_ID || '5vYER4u7ZKc3KktH2Z5NaMLWNmM6WjuM'),
            'AUTH0_REDIRECT_URI': JSON.stringify(process.env.AUTH0_REDIRECT_URI || (MODE == 'production' ? 'http://localhost:8000' : 'http://localhost:3000')),
            'AUTH0_AUDIENCE': JSON.stringify(process.env.AUTH0_AUDIENCE || 'https://playground.ethanfrei.com'),
            // 'BASE_API_URL': JSON.stringify(process.env.BASE_API_URL || (MODE == 'production' ? '/api' : 'http://playground.ethanfrei.com/api'))
            'BASE_API_URL': JSON.stringify(process.env.BASE_API_URL || (MODE == 'production' ? '/api' : 'http://localhost:8000/api'))
        })
    ],
    resolve: {
        modules: [__dirname, "node_modules"],
        extensions: [".js", ".elm", ".scss", ".png"]
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.scss$/,
                exclude: [/elm-stuff/, /node_modules/],
                use: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.css$/,
                exclude: [/elm-stuff/, /node_modules/],
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                exclude: [/elm-stuff/, /node_modules/],
                loader: "url-loader",
                options: {
                    limit: 10000,
                    mimetype: "application/font-woff"
                }
            },
            {
                test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                exclude: [/elm-stuff/, /node_modules/],
                loader: "file-loader"
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loader: "file-loader"
            }
        ]
    }
};

if (MODE === "development") {
    console.log("Building for dev...");
    module.exports = merge(common, {
        plugins: [
            // Suggested for hot-loading
            new webpack.NamedModulesPlugin(),
            // Prevents compilation errors causing the hot loader to lose state
            new webpack.NoEmitOnErrorsPlugin(),
            new webpack.HotModuleReplacementPlugin()
        ],
        module: {
            rules: [
                {
                    test: /\.elm$/,
                    exclude: [/elm-stuff/, /node_modules/],
                    use: [
                        // {
                        //     loader: "elm-hot-loader"
                        // },
                        {
                            loader: "elm-webpack-loader",
                            // add Elm's debug overlay to output
                            options: {
                                debug: true,
                                forceWatch: true
                            }
                        }
                    ]
                }
            ]
        },
        devServer: {
            inline: true,
            stats: "errors-only",
            contentBase: __dirname,
            // For SPAs: serve index.html in place of 404 responses
            historyApiFallback: true,
            hot: true
        },
    });
}

if (MODE === "production") {
    console.log("Building for Production...");
    module.exports = merge(common, {
        plugins: [
            // Delete everything from output directory and report to user
            new CleanWebpackPlugin(["api/static"], {
                root: path.join(__dirname, ".."),
                exclude: [],
                verbose: true,
                dry: false
            }),
            new CopyWebpackPlugin([
                {
                    from: "assets",
                    to: "assets/"
                }
            ]),
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: "assets/css/[name]-[hash].css"
            })
        ],
        module: {
            rules: [
                {
                    test: /\.elm$/,
                    exclude: [/elm-stuff/, /node_modules/],
                    use: {
                        loader: 'elm-webpack-loader',
                        options: {
                            optimize: true
                        }
                    }
                },
                {
                    test: /\.css$/,
                    exclude: [/elm-stuff/, /node_modules/],
                    use: [ MiniCssExtractPlugin.loader, "css-loader" ]
                },
                {
                    test: /\.scss$/,
                    exclude: [/elm-stuff/, /node_modules/],
                    use: [ MiniCssExtractPlugin.loader, "css-loader", "sass-loader" ]
                }
            ]
        }
    });
}
