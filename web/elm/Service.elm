module Service exposing (fetchMcStatus, turnServerOff, turnServerOn)

import Http
import Json.Decode as Decode
import Models exposing (Model, Msg(..), Route(..))
import RemoteData


fetchMcStatus : String -> Cmd Msg
fetchMcStatus baseApiUrl =
    Http.get
        { url = baseApiUrl ++ "/mc"
        , expect = Http.expectJson (RemoteData.fromResult >> OnMcStatusReceived) Decode.bool
        }


turnServerOn : String -> Cmd Msg
turnServerOn baseApiUrl =
    Http.post
        { url = baseApiUrl ++ "/mc/on"
        , expect = Http.expectWhatever OnMcTurnedServerOn
        , body = Http.emptyBody
        }


turnServerOff : String -> Cmd Msg
turnServerOff baseApiUrl =
    Http.post
        { url = baseApiUrl ++ "/mc/off"
        , expect = Http.expectWhatever OnMcTurnedServerOff
        , body = Http.emptyBody
        }
