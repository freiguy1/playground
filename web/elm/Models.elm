module Models exposing (McModel, Model, Msg(..), Route(..))

import Authentication
import Browser
import Browser.Navigation as Nav
import Gutenberg.Models
import Http
import RemoteData exposing (WebData)
import Showcase.Models
import Url exposing (Url)


type alias Model =
    { route : Route
    , authModel : Authentication.Model
    , baseApiUrl : String
    , key : Nav.Key
    , showcaseModel : Showcase.Models.BaseModel
    , gutenbergModel : Gutenberg.Models.BaseModel
    , mcModel : McModel
    }


type alias McModel =
    { status : WebData Bool
    , errorMessage : Maybe String
    , serverChangingStatus : Bool
    }


type Route
    = Index
    | Showcase Showcase.Models.Route
    | Gutenberg Gutenberg.Models.Route
    | Mc
    | NotFound


type Msg
    = OnUrlChange Url -- don't call directly
    | OnUrlRequest Browser.UrlRequest
    | AuthenticationMsg Authentication.Msg
    | Noop
    | OnShowcaseMsg Showcase.Models.Msg
    | OnGutenbergMsg Gutenberg.Models.Msg
    | OnMcStatusReceived (WebData Bool)
    | OnMcTurnedServerOn (Result Http.Error ())
    | OnMcTurnedServerOff (Result Http.Error ())
    | TurnMcServerOnClicked
    | TurnMcServerOffClicked
