module Util exposing
    ( isJust
    , maybeToVisibility
    )

import Bootstrap.Modal


isJust : Maybe a -> Bool
isJust maybe =
    maybe
        |> Maybe.map (\_ -> True)
        |> Maybe.withDefault False


maybeToVisibility : Maybe t -> Bootstrap.Modal.Visibility
maybeToVisibility maybe =
    case maybe of
        Just _ ->
            Bootstrap.Modal.shown

        _ ->
            Bootstrap.Modal.hidden
