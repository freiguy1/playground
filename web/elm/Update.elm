module Update exposing (update)

import Authentication
import Browser
import Browser.Dom exposing (setViewport)
import Browser.Navigation as Nav
import Gutenberg.Adapter
import Models exposing (Model, Msg(..))
import RemoteData
import Routing exposing (getRouteAndCmdFromLocation)
import Service
import Showcase.Adapter
import Task
import Url


scrollToTop : Task.Task x ()
scrollToTop =
    setViewport 0 0
        |> Task.onError (\_ -> Task.succeed ())


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnUrlChange location ->
            let
                scrollCmd =
                    Task.attempt (\_ -> Noop) scrollToTop

                ( route, cmd ) =
                    getRouteAndCmdFromLocation location model

                batchCmd =
                    Cmd.batch [ cmd, scrollCmd ]
            in
            ( { model | route = route }, batchCmd )

        OnUrlRequest urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        AuthenticationMsg authMsg ->
            let
                ( authModel, cmd ) =
                    Authentication.update model.baseApiUrl authMsg model.authModel
            in
            ( { model | authModel = authModel }, Cmd.map AuthenticationMsg cmd )

        Noop ->
            ( model, Cmd.none )

        OnShowcaseMsg showcaseMsg ->
            Showcase.Adapter.handleUpdate model showcaseMsg

        OnGutenbergMsg gutenbergMsg ->
            Gutenberg.Adapter.handleUpdate model gutenbergMsg

        OnMcStatusReceived response ->
            let
                oldModel =
                    model.mcModel

                mcModel =
                    { oldModel | status = response }
            in
            ( { model | mcModel = mcModel }, Cmd.none )

        OnMcTurnedServerOn (Ok _) ->
            let
                oldModel =
                    model.mcModel

                mcModel =
                    { oldModel
                        | status = RemoteData.Success True
                        , errorMessage = Nothing
                        , serverChangingStatus = False
                    }
            in
            ( { model | mcModel = mcModel }, Cmd.none )

        OnMcTurnedServerOn (Err _) ->
            let
                oldModel =
                    model.mcModel

                mcModel =
                    { oldModel
                        | errorMessage = Just "There was a problem turning the server on, sorry. Tell Ethan."
                        , serverChangingStatus = False
                    }
            in
            ( { model | mcModel = mcModel }, Cmd.none )

        OnMcTurnedServerOff (Ok _) ->
            let
                oldModel =
                    model.mcModel

                mcModel =
                    { oldModel
                        | status = RemoteData.Success False
                        , errorMessage = Nothing
                        , serverChangingStatus = False
                    }
            in
            ( { model | mcModel = mcModel }, Cmd.none )

        OnMcTurnedServerOff (Err _) ->
            let
                oldModel =
                    model.mcModel

                mcModel =
                    { oldModel
                        | errorMessage = Just "There was a problem turning the server off, sorry. Tell Ethan."
                        , serverChangingStatus = False
                    }
            in
            ( { model | mcModel = mcModel }, Cmd.none )

        TurnMcServerOnClicked ->
            let
                oldModel =
                    model.mcModel

                mcModel =
                    { oldModel | serverChangingStatus = True }
            in
            ( { model | mcModel = mcModel }, Service.turnServerOn model.baseApiUrl )

        TurnMcServerOffClicked ->
            let
                oldModel =
                    model.mcModel

                mcModel =
                    { oldModel | serverChangingStatus = True }
            in
            ( { model | mcModel = mcModel }, Service.turnServerOff model.baseApiUrl )
