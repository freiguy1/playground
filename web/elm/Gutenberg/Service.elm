module Gutenberg.Service exposing (searchBooks)

import Gutenberg.Models exposing (SearchResults, Msg(..), Author, SearchResult) 
import Http
import Json.Decode as Decode
import Json.Decode.Pipeline exposing (required)
import RemoteData


gutendexBaseUrl : String
gutendexBaseUrl =
    "https://gutendex.com/books"


searchBooks : String -> Cmd Msg
searchBooks query =
    Http.get
        { url = gutendexBaseUrl ++ "?mime_type=text%2Fplain&search=" ++ query
        , expect =
            Http.expectJson
                (RemoteData.fromResult >> SearchResultsReceived)
                searchResultsDecoder
        }


searchResultsDecoder : Decode.Decoder SearchResults
searchResultsDecoder =
    Decode.succeed SearchResults
        |> required "count" Decode.int
        |> required "results" (Decode.list searchResultDecoder)


searchResultDecoder : Decode.Decoder SearchResult
searchResultDecoder =
    Decode.succeed SearchResult
        |> required "id" Decode.int
        |> required "title" Decode.string
        |> required "authors" (Decode.list authorDecoder)
        |> required "download_count" Decode.int
        |> required "formats" (Decode.keyValuePairs Decode.string)


authorDecoder : Decode.Decoder Author
authorDecoder =
    Decode.succeed Author
        |> required "name" Decode.string
        |> required "birth_year" (Decode.nullable Decode.int)
        |> required "death_year" (Decode.nullable Decode.int)
