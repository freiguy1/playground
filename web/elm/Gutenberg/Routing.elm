module Gutenberg.Routing exposing (getCmdFromRouteAndModel, getViewFromRoute, matchers)

import Gutenberg.Index as IndexView
import Gutenberg.Models exposing (Route(..), Model, Msg(..))
import Html exposing (Html)
import Url.Parser exposing (Parser, map, oneOf, top)


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map Index top
        ]


getCmdFromRouteAndModel : Route -> Model -> Cmd Msg
getCmdFromRouteAndModel route model =
    case route of
        Index ->
            IndexView.initCmd model


getViewFromRoute : Route -> (Model -> { title : String, content : Html Msg })
getViewFromRoute route =
    case route of
        Index ->
            IndexView.view
