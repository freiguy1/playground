module Gutenberg.Adapter exposing (gutenbergToRootModel, gutenbergToRootView, handleUpdate, rootToGutenbergModel)

import Gutenberg.Models
import Gutenberg.Update
import Html exposing (Html)
import Models as RootModels


-- MODEL ADAPTERS


rootToGutenbergModel :
    RootModels.Model
    -> Gutenberg.Models.Model
rootToGutenbergModel rootModel =
    let
        gutenbergRoute =
            case rootModel.route of
                RootModels.Gutenberg r ->
                    r

                _ ->
                    -- default. probably won't get hit
                    Gutenberg.Models.Index
    in
    { key = rootModel.key
    , authModel = rootModel.authModel
    , baseApiUrl = rootModel.baseApiUrl ++ "/gutenberg"
    , route = gutenbergRoute
    , searchText = rootModel.gutenbergModel.searchText
    , searchResults = rootModel.gutenbergModel.searchResults
    }


gutenbergToRootModel : Gutenberg.Models.Model -> Gutenberg.Models.BaseModel
gutenbergToRootModel model =
    { searchText = model.searchText
    , searchResults = model.searchResults
    }



-- UPDATE ADAPTERS


handleUpdate : RootModels.Model -> Gutenberg.Models.Msg -> ( RootModels.Model, Cmd RootModels.Msg )
handleUpdate rootModel gutenbergMsg =
    let
        wrappedModel =
            rootToGutenbergModel rootModel

        ( gutenbergModel, cmd ) =
            Gutenberg.Update.update gutenbergMsg wrappedModel

        newGutenbergModel =
            gutenbergToRootModel gutenbergModel

        newRootCmd =
            Cmd.map RootModels.OnGutenbergMsg cmd
    in
    ( { rootModel | gutenbergModel = newGutenbergModel }, newRootCmd )



-- VIEW ADPTERS


gutenbergToRootView :
    (Gutenberg.Models.Model -> { title : String, content : Html Gutenberg.Models.Msg })
    -> (RootModels.Model -> { title : String, content : Html RootModels.Msg })
gutenbergToRootView gutenbergView =
    \gutenbergModel ->
        gutenbergView (rootToGutenbergModel gutenbergModel)
            |> convertViewOutput


convertViewOutput :
    { title : String, content : Html Gutenberg.Models.Msg }
    -> { title : String, content : Html RootModels.Msg }
convertViewOutput gutenbergStuff =
    { title = gutenbergStuff.title
    , content = Html.map RootModels.OnGutenbergMsg gutenbergStuff.content
    }
