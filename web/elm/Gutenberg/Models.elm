module Gutenberg.Models exposing (Author, BaseModel, Model, Msg(..), Route(..), SearchResult, SearchResults, init)

import Authentication
import Browser.Navigation as Nav
import RemoteData exposing (WebData)



-- MODEL
-- contains everything in BaseModel plus global things


type alias Model =
    { key : Nav.Key
    , baseApiUrl : String
    , route : Route
    , authModel : Authentication.Model
    , searchText : String
    , searchResults : WebData SearchResults
    }


type alias BaseModel =
    { searchText : String
    , searchResults : WebData SearchResults
    }


init : BaseModel
init =
    { searchText = ""
    , searchResults = RemoteData.NotAsked
    }


type alias SearchResults =
    { count : Int
    , results : List SearchResult
    }


type alias SearchResult =
    { id : Int
    , title : String
    , authors : List Author
    , downloadCount : Int
    , formats : List ( String, String )
    }


type alias Author =
    { name : String
    , birthYear : Maybe Int
    , deathYear : Maybe Int
    }



-- MSG


type Msg
    = Noop
    | SearchTextChanged String
    | SearchTextKeyDown Int
    | SearchResultsReceived (WebData SearchResults)



-- ROUTE


type Route
    = Index
