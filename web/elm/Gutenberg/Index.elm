module Gutenberg.Index exposing (initCmd, view)

import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Gutenberg.Models exposing (Model, Msg(..), SearchResult, Author)
import Html exposing (Attribute, Html, a, div, input, text)
import Html.Attributes exposing (class, href, placeholder, target, value)
import Html.Events exposing (keyCode, on, onInput)
import Json.Decode as Decode
import RemoteData


initCmd : Model -> Cmd Msg
initCmd _ =
    Cmd.none


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "Gutenberg Reader"
    , content =
        div []
            [ renderSearch model
            , renderSearchResults model
            ]
    }


renderSearchResults : Model -> Html Msg
renderSearchResults model =
    case model.searchResults of
        RemoteData.Success searchResults ->
            div [ class "search-results" ] (searchResults.results |> List.map renderSearchResult)

        RemoteData.Loading ->
            div [] [ text "Loading..." ]

        RemoteData.NotAsked ->
            div [] [ text "Search for some books, friend." ]

        RemoteData.Failure _ ->
            div [] [ text "An error occured. Try searching again" ]


renderSearchResult : SearchResult -> Html Msg
renderSearchResult sr =
    Card.config []
        |> Card.block []
            [ Block.titleH3 [] [ text sr.title ]
            , Block.text [] [ text (authorsText sr.authors) ]
            , Block.text [] [ text (downloadCountText sr.downloadCount) ]
            , Block.custom (renderFormats sr.formats)
            ]
        |> Card.view


renderFormats : List ( String, String ) -> Html Msg
renderFormats formats =
    let
        renderFormat =
            \format ->
                a
                    [ href (Tuple.second format), target "_blank", class "mr-3" ]
                    [ text (format |> Tuple.first |> prettyFormat) ]

        aTags =
            List.map renderFormat formats
    in
    div [] aTags


prettyFormat : String -> String
prettyFormat format =
    case format of
        "text/plain; charset=utf-8" ->
            "plain text (utf-8)"

        "text/plain" ->
            "plain text"

        "text/plain; charset=us-ascii" ->
            "plain text (ascii)"

        "text/plain; charset=iso-8859-1" ->
            "plain text (iso-8859)"

        "text/html; charset=utf-8" ->
            "html"

        "text/html" ->
            "html"

        "text/html; charset=iso-8859-1" ->
            "html"

        "text/html; charset=us-ascii" ->
            "html"

        "application/rdf+xml" ->
            "rdf xml"

        "application/epub+zip" ->
            "epub"

        "application/x-mobipocket-ebook" ->
            "kindle"

        "application/pdf" ->
            "pdf"

        "application/zip" ->
            "zip"

        _ ->
            format


downloadCountText : Int -> String
downloadCountText downloadCount =
    "downloaded "
        ++ prettyInt downloadCount
        ++ (if downloadCount == 1 then
                " time"

            else
                " times"
           )


prettyInt : Int -> String
prettyInt num =
    let
        numSections =
            (num |> toFloat |> logBase 10 |> floor) // 3

        sections =
            List.range 0 numSections
                |> List.map (\i -> num // (1000 ^ (numSections - i)) |> modBy 1000)
                |> List.map String.fromInt
    in
    case sections of
        [] ->
            ""

        head :: tail ->
            String.join "," (head :: List.map (String.padLeft 3 '0') tail)


authorsText : List Author -> String
authorsText authors =
    "by " ++ String.join " & " (List.map authorText authors)


authorText : Author -> String
authorText author =
    let
        birthYear =
            author.birthYear |> Maybe.map String.fromInt |> Maybe.withDefault "??"

        deathYear =
            author.deathYear |> Maybe.map String.fromInt |> Maybe.withDefault "??"
    in
    author.name ++ " (" ++ birthYear ++ " - " ++ deathYear ++ ")"


renderSearch : Model -> Html Msg
renderSearch model =
    input
        [ placeholder "Search here"
        , value model.searchText
        , onInput SearchTextChanged
        , onKeyDown SearchTextKeyDown
        ]
        []


onKeyDown : (Int -> msg) -> Attribute msg
onKeyDown tagger =
    on "keydown" (Decode.map tagger keyCode)
