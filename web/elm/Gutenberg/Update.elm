module Gutenberg.Update exposing (update)

import Gutenberg.Models exposing (Msg(..), Model)
import Gutenberg.Service exposing (searchBooks)
import RemoteData


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Noop ->
            ( model, Cmd.none )

        SearchTextChanged searchText ->
            ( { model | searchText = searchText }, Cmd.none )

        SearchTextKeyDown keyCode ->
            if keyCode == 13 then
                let
                    cmd =
                        searchBooks model.searchText

                    newModel =
                        { model | searchResults = RemoteData.Loading }
                in
                ( newModel, cmd )

            else
                ( model, Cmd.none )

        SearchResultsReceived response ->
            ( { model | searchResults = response }, Cmd.none )
