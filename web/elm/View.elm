module View exposing (indexView, mcView, notFoundView)

import Authentication
import Bootstrap.Alert as Alert
import Bootstrap.Button as Button
import Html exposing (Html, a, button, div, h1, li, p, text, ul)
import Html.Attributes exposing (class, href)
import Html.Events exposing (onClick)
import Models exposing (McModel, Model, Msg(..))
import RemoteData exposing (WebData)


indexView : Model -> { title : String, content : Html Msg }
indexView model =
    { title = "Playground"
    , content =
        div [ class "container mv-3" ]
            [ h1 [] [ text "Playground" ]
            , div []
                (case Authentication.tryGetUserProfile model.authModel of
                    Nothing ->
                        [ p [] [ text "Hello anonymous being." ] ]

                    Just user ->
                        [ p [] [ text ("Hello, " ++ user.email ++ "!") ] ]
                )
            , button
                [ class "btn btn-primary"
                , onClick
                    (AuthenticationMsg
                        (if Authentication.isLoggedIn model.authModel then
                            Authentication.LogOut

                         else
                            Authentication.ShowLogIn
                        )
                    )
                ]
                [ text
                    (if Authentication.isLoggedIn model.authModel then
                        "Log Out"

                     else
                        "Log In"
                    )
                ]
            , ul []
                [ li [] [ a [ href "/showcase" ] [ text "Showcase" ] ]
                , li [] [ a [ href "/gutenberg" ] [ text "Gutenberg Reader" ] ]
                ]
            ]
    }


notFoundView : { title : String, content : Html Msg }
notFoundView =
    { title = "Not Found"
    , content =
        div [ class "container mv-3" ]
            [ p [] [ text "Page not found, sorry." ]
            , a [ href "/" ] [ text "Back to Playground" ]
            ]
    }


mcView : McModel -> { title : String, content : Html Msg }
mcView model =
    { title = "Minecraft on demand"
    , content =
        div [ class "container mv-3" ]
            [ h1 [] [ text "Tomah boys minecraft server." ]
            , mcStatusView model.status
            , mcButtonView model
            , mcNotesView model.status
            ]
    }


mcStatusView : WebData Bool -> Html Msg
mcStatusView status =
    case status of
        RemoteData.Loading ->
            div [] [ text "Loading status" ]

        RemoteData.Failure e ->
            div [] [ text "An error happened when getting status" ]

        RemoteData.Success False ->
            Alert.simpleDark [] [ text "Currently the minecraft server is off" ]

        RemoteData.Success True ->
            Alert.simpleSuccess [] [ text "Currently the minecraft server is on!" ]

        RemoteData.NotAsked ->
            div [] [ text "This should never happen" ]


mcButtonView : McModel -> Html Msg
mcButtonView model =
    let
        classes =
            "my-4 text-center"

        button =
            case ( model.serverChangingStatus, model.status ) of
                ( True, RemoteData.Success True ) ->
                    Button.button [ Button.danger, Button.disabled True, Button.large, Button.attrs [ class classes ] ] [ text "Hold yer britches" ]

                ( True, RemoteData.Success False ) ->
                    Button.button [ Button.success, Button.disabled True, Button.large, Button.attrs [ class classes ] ] [ text "Hold yer britches" ]

                ( False, RemoteData.Success True ) ->
                    Button.button [ Button.danger, Button.large, Button.attrs [ onClick TurnMcServerOffClicked, class classes ] ] [ text "Turn off" ]

                ( False, RemoteData.Success False ) ->
                    Button.button [ Button.success, Button.large, Button.attrs [ onClick TurnMcServerOnClicked, class classes ] ] [ text "Turn on" ]

                _ ->
                    div [] []
    in
    div [ class "d-flex justify-content-center mb-4" ] [ button ]


mcNotesView : WebData Bool -> Html Msg
mcNotesView status =
    case status of
        RemoteData.Success _ ->
            div []
                [ p [] [ text "A few things to keep in mind:" ]
                , p [] [ text "You won't be able to connect in the game for maybe 2-3 min from starting up. It takes a bit to get the juices flowing." ]
                , p [] [ text "You don't actually need to turn it off when you're done, I set up a thing which will turn it off when everyone's logged off for 10 min." ]
                , p [] [ text "Don't rapidly turn it on and off, that probably won't be great." ]
                ]

        _ ->
            div [] []
