module Showcase.Service exposing (createItem, deleteItem, fetchItems, updateItem, updateItemImages)

import Auth0
import Authentication
import Http
import Json.Decode as Decode
import Json.Decode.Pipeline exposing (required)
import Json.Encode as Encode
import RemoteData
import Showcase.Models exposing (Item, ItemForm, ItemImage, Msg(..))


fetchItems : String -> Cmd Msg
fetchItems baseApiUrl =
    Http.get
        { url = baseApiUrl ++ itemsUrl
        , expect = Http.expectJson (RemoteData.fromResult >> OnReceivedItems) itemsDecoder
        }


itemsUrl : String
itemsUrl =
    "/items"


itemsDecoder : Decode.Decoder (List Item)
itemsDecoder =
    Decode.list itemDecoder


itemDecoder : Decode.Decoder Item
itemDecoder =
    Decode.succeed Item
        |> required "id" Decode.int
        |> required "name" Decode.string
        |> required "summary" Decode.string
        |> required "description" Decode.string
        |> required "banner_image_url" (Decode.nullable Decode.string)
        |> required "price" (Decode.nullable Decode.string)
        |> required "quantity_available" Decode.int
        |> required "images" (Decode.list itemImageDecoder)


itemImageDecoder : Decode.Decoder ItemImage
itemImageDecoder =
    Decode.succeed ItemImage
        |> required "image_url" Decode.string
        |> required "description" (Decode.nullable Decode.string)


createItem : ItemForm -> Authentication.Model -> String -> Cmd Msg
createItem item authModel baseApiUrl =
    case authModel.state of
        Auth0.LoggedOut ->
            Cmd.none

        Auth0.LoggedIn u ->
            createItemCmd item u baseApiUrl


createItemCmd : ItemForm -> Auth0.LoggedInUser -> String -> Cmd Msg
createItemCmd item user baseApiUrl =
    let
        headers =
            [ Http.header "Authorization" ("Bearer " ++ user.token) ]

        request =
            { method = "POST"
            , headers = headers
            , url = baseApiUrl ++ itemsUrl
            , body = Http.jsonBody (itemFormEncoder item)
            , expect = Http.expectJson (RemoteData.fromResult >> FinishedCreatingItem) itemDecoder
            , timeout = Nothing
            , tracker = Nothing
            }
    in
    Http.request request


updateItem : Int -> ItemForm -> Authentication.Model -> String -> Cmd Msg
updateItem id item authModel baseApiUrl =
    case authModel.state of
        Auth0.LoggedOut ->
            Cmd.none

        Auth0.LoggedIn u ->
            updateItemCmd id item u baseApiUrl


updateItemCmd : Int -> ItemForm -> Auth0.LoggedInUser -> String -> Cmd Msg
updateItemCmd id item user baseApiUrl =
    let
        headers =
            [ Http.header "Authorization" ("Bearer " ++ user.token) ]

        request =
            { method = "PUT"
            , headers = headers
            , url = baseApiUrl ++ itemsUrl ++ "/" ++ String.fromInt id
            , body = Http.jsonBody (itemFormEncoder item)
            , expect = Http.expectJson (RemoteData.fromResult >> FinishedUpdatingItem) itemDecoder
            , timeout = Nothing
            , tracker = Nothing
            }
    in
    Http.request request


deleteItem : Int -> Authentication.Model -> String -> Cmd Msg
deleteItem id authModel baseApiUrl =
    case authModel.state of
        Auth0.LoggedOut ->
            Cmd.none

        Auth0.LoggedIn u ->
            deleteItemCmd id u baseApiUrl


deleteItemCmd : Int -> Auth0.LoggedInUser -> String -> Cmd Msg
deleteItemCmd id user baseApiUrl =
    let
        headers =
            [ Http.header "Authorization" ("Bearer " ++ user.token) ]

        request =
            { method = "DELETE"
            , headers = headers
            , url = baseApiUrl ++ itemsUrl ++ "/" ++ String.fromInt id
            , body = Http.emptyBody
            , expect = Http.expectWhatever FinishedDeletingItem
            , timeout = Nothing
            , tracker = Nothing
            }
    in
    Http.request request


updateItemImages : Int -> List ItemImage -> Authentication.Model -> String -> Cmd Msg
updateItemImages id images authModel baseApiUrl =
    case authModel.state of
        Auth0.LoggedOut ->
            Cmd.none

        Auth0.LoggedIn u ->
            updateItemImagesCmd id images u baseApiUrl


updateItemImagesCmd : Int -> List ItemImage -> Auth0.LoggedInUser -> String -> Cmd Msg
updateItemImagesCmd id images user baseApiUrl =
    let
        headers =
            [ Http.header "Authorization" ("Bearer " ++ user.token) ]

        request =
            { method = "PUT"
            , headers = headers
            , url = baseApiUrl ++ itemsUrl ++ "/" ++ String.fromInt id ++ "/images"
            , body = Http.jsonBody (images |> Encode.list itemImageEncoder)
            , expect =
                Http.expectJson
                    (RemoteData.fromResult >> FinishedUpdatingItemImages id)
                    (Decode.list itemImageDecoder)
            , timeout = Nothing
            , tracker = Nothing
            }
    in
    Http.request request


itemImageEncoder : ItemImage -> Encode.Value
itemImageEncoder i =
    Encode.object
        [ ( "image_url", encodeMaybeNullString i.imageUrl )
        , ( "description", encodeMaybeNullString (i.description |> Maybe.withDefault "") )
        ]


itemFormEncoder : ItemForm -> Encode.Value
itemFormEncoder i =
    Encode.object
        [ ( "name", encodeMaybeNullString i.name )
        , ( "summary", encodeMaybeNullString i.summary )
        , ( "description", encodeMaybeNullString i.description )
        , ( "banner_image_url", encodeMaybeNullString i.bannerImageUrl )
        , ( "price", encodeMaybeNullString i.price )
        , ( "quantity_available", encodeIntFromStringWithDefault 0 i.quantityAvailable )
        ]


encodeIntFromStringWithDefault : Int -> String -> Encode.Value
encodeIntFromStringWithDefault default s =
    s
        |> String.toInt
        |> Maybe.withDefault default
        |> Encode.int


encodeMaybeNullString : String -> Encode.Value
encodeMaybeNullString s =
    if String.isEmpty s then
        Encode.null

    else
        Encode.string s
