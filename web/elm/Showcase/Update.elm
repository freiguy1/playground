module Showcase.Update exposing (update)

import Array
import RemoteData
import Showcase.Models exposing (AddEditItemModel, EditImagesForm, Item, ItemForm, ItemImage, Model, Msg(..))
import Showcase.Service as Service


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnReceivedItems wdItems ->
            ( { model | items = wdItems }, Cmd.none )

        Noop ->
            ( model, Cmd.none )

        ShowConfirmDeleteItemModal item ->
            let
                newModel =
                    { model | itemToDelete = Just item }
            in
            ( newModel, Cmd.none )

        CloseConfirmDeleteItemModal ->
            let
                newModel =
                    { model | itemToDelete = Nothing }
            in
            ( newModel, Cmd.none )

        CloseAddEditItemModal ->
            let
                newModel =
                    { model | addEditItemModel = Nothing }
            in
            ( newModel, Cmd.none )

        StartEditItem item ->
            let
                form =
                    ItemForm
                        item.name
                        item.summary
                        item.description
                        (item.bannerImageUrl |> Maybe.withDefault "")
                        (item.price |> Maybe.withDefault "")
                        (item.quantityAvailable |> String.fromInt)

                newAddEditItemModel =
                    AddEditItemModel form (Just item)

                newModel =
                    { model | addEditItemModel = Just newAddEditItemModel }
            in
            ( newModel, Cmd.none )

        StartAddItem ->
            let
                form =
                    ItemForm "" "" "" "" "" "0"

                newAddEditItemModel =
                    AddEditItemModel form Nothing

                newModel =
                    { model | addEditItemModel = Just newAddEditItemModel }
            in
            ( newModel, Cmd.none )

        ItemFormInput fun input ->
            let
                newAddEditItemModel =
                    case model.addEditItemModel of
                        Just addEditItemModel ->
                            Just { addEditItemModel | itemForm = fun addEditItemModel.itemForm input }

                        Nothing ->
                            Nothing
            in
            ( { model | addEditItemModel = newAddEditItemModel }, Cmd.none )

        SaveAddEditItemClicked ->
            case model.addEditItemModel of
                Just addEditItemModel ->
                    case addEditItemModel.editItem of
                        Just editItem ->
                            ( model, Service.updateItem editItem.id addEditItemModel.itemForm model.authModel model.baseApiUrl )

                        Nothing ->
                            ( model, Service.createItem addEditItemModel.itemForm model.authModel model.baseApiUrl )

                Nothing ->
                    ( model, Cmd.none )

        FinishedCreatingItem (RemoteData.Success i) ->
            let
                newItems =
                    case model.items of
                        RemoteData.Success items ->
                            items
                                |> Array.fromList
                                |> Array.push i
                                |> Array.toList
                                |> RemoteData.Success

                        items ->
                            items

                newModel =
                    { model
                        | items = newItems
                        , addEditItemModel = Nothing
                    }
            in
            ( newModel, Cmd.none )

        FinishedCreatingItem _ ->
            -- Should probably account for if this errors out
            ( model, Cmd.none )

        FinishedUpdatingItem (RemoteData.Success i) ->
            let
                newItems =
                    case model.items of
                        RemoteData.Success items ->
                            items
                                |> List.map (\i2 -> switchItemsIfIdsAreEqual i2 i)
                                |> RemoteData.Success

                        items ->
                            items

                newModel =
                    { model
                        | items = newItems
                        , addEditItemModel = Nothing
                    }
            in
            ( newModel, Cmd.none )

        FinishedUpdatingItem _ ->
            -- Should probably account for if this errors out
            ( model, Cmd.none )

        FinishedDeletingItem (Ok _) ->
            let
                itemIdToDelete =
                    model.itemToDelete
                        |> Maybe.map (\itd -> itd.id)

                newItems =
                    case ( model.items, itemIdToDelete ) of
                        ( RemoteData.Success items, Just id ) ->
                            items
                                |> List.filter (\i -> i.id /= id)
                                |> RemoteData.Success

                        _ ->
                            model.items

                newModel =
                    { model
                        | items = newItems
                        , itemToDelete = Nothing
                    }
            in
            ( newModel, Cmd.none )

        FinishedDeletingItem _ ->
            -- Should probably account for if this errors out
            ( model, Cmd.none )

        DeleteItemClicked ->
            case model.itemToDelete of
                Just i ->
                    ( model
                    , Service.deleteItem i.id model.authModel model.baseApiUrl
                    )

                Nothing ->
                    ( model, Cmd.none )

        StartEditItemImages item ->
            let
                form =
                    EditImagesForm item.id (Array.fromList item.images)

                forms =
                    form :: model.editImagesForms
            in
            ( { model | editImagesForms = forms }, Cmd.none )

        CancelEditItemImages item ->
            let
                forms =
                    List.filter (\f -> f.itemId /= item.id) model.editImagesForms
            in
            ( { model | editImagesForms = forms }, Cmd.none )

        ImageFormInput itemId imageIndex fn input ->
            let
                forms =
                    model.editImagesForms
                        |> List.map
                            (\f ->
                                if f.itemId == itemId then
                                    updateEditImagesForm f imageIndex fn input

                                else
                                    f
                            )
            in
            ( { model | editImagesForms = forms }, Cmd.none )

        DeleteItemImage itemId imageIndex ->
            let
                forms =
                    model.editImagesForms
                        |> List.map
                            (\f ->
                                if f.itemId == itemId then
                                    deleteItemImage f imageIndex

                                else
                                    f
                            )
            in
            ( { model | editImagesForms = forms }, Cmd.none )

        AddItemImage itemId ->
            let
                forms =
                    model.editImagesForms
                        |> List.map
                            (\f ->
                                if f.itemId == itemId then
                                    { f | images = Array.push (ItemImage "" (Just "")) f.images }

                                else
                                    f
                            )
            in
            ( { model | editImagesForms = forms }, Cmd.none )

        SaveItemImages itemId images ->
            let
                cmd =
                    Service.updateItemImages itemId images model.authModel model.baseApiUrl
            in
            ( model, cmd )

        FinishedUpdatingItemImages itemId (RemoteData.Success images) ->
            let
                newItems =
                    case model.items of
                        RemoteData.Success items ->
                            items
                                |> List.map (\i2 -> switchImagesIfIdsAreEqual itemId images i2)
                                |> RemoteData.Success

                        items ->
                            items

                newEditImagesForms =
                    model.editImagesForms
                        |> List.filter (\f -> f.itemId /= itemId)

                newModel =
                    { model
                        | items = newItems
                        , editImagesForms = newEditImagesForms
                    }
            in
            ( newModel, Cmd.none )

        FinishedUpdatingItemImages _ _ ->
            -- Should probably account for errors
            ( model, Cmd.none )


deleteItemImage : EditImagesForm -> Int -> EditImagesForm
deleteItemImage form imageIndex =
    let
        images =
            form.images
                |> Array.toIndexedList
                |> List.filter (\i -> Tuple.first i /= imageIndex)
                |> List.map Tuple.second
                |> Array.fromList
    in
    { form | images = images }


updateEditImagesForm : EditImagesForm -> Int -> (ItemImage -> String -> ItemImage) -> String -> EditImagesForm
updateEditImagesForm form imageIndex fn input =
    let
        updatedImage =
            Array.get imageIndex form.images
                |> Maybe.map (\image -> fn image input)
    in
    case updatedImage of
        Just image ->
            { form | images = Array.set imageIndex image form.images }

        Nothing ->
            form


switchItemsIfIdsAreEqual : Item -> Item -> Item
switchItemsIfIdsAreEqual original new =
    if original.id == new.id then
        new

    else
        original


switchImagesIfIdsAreEqual : Int -> List ItemImage -> Item -> Item
switchImagesIfIdsAreEqual itemId images original =
    if original.id == itemId then
        { original | images = images }

    else
        original
