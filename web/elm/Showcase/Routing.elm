module Showcase.Routing exposing (getCmdFromRouteAndModel, getViewFromRoute, matchers)

import Html exposing (Html)
import Showcase.Models exposing (Model, Msg(..), Route(..))
import Showcase.Views.About as AboutView
import Showcase.Views.Admin as AdminView
import Showcase.Views.Index as IndexView
import Showcase.Views.ItemDetail as ItemDetailView
import Url.Parser exposing (Parser, int, map, oneOf, s, top)


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map Index top
        , map Admin (s "admin")
        , map About (s "about")
        , map ItemDetail int
        ]


getCmdFromRouteAndModel : Route -> Model -> Cmd Msg
getCmdFromRouteAndModel route model =
    case route of
        Index ->
            IndexView.initCmd model

        Admin ->
            AdminView.initCmd model

        ItemDetail _ ->
            ItemDetailView.initCmd model

        About ->
            Cmd.none


getViewFromRoute : Route -> (Model -> { title : String, content : Html Msg })
getViewFromRoute route =
    case route of
        Index ->
            IndexView.view

        Admin ->
            AdminView.view

        ItemDetail id ->
            ItemDetailView.view id

        About ->
            AboutView.view
