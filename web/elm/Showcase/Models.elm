module Showcase.Models exposing (AddEditItemModel, BaseModel, EditImagesForm, Item, ItemForm, ItemImage, Model, Msg(..), Route(..), init)

import Array exposing (Array)
import Authentication
import Browser.Navigation as Nav
import Http
import RemoteData exposing (WebData)



-- MODEL
-- contains everything in BaseModel plus global things


type alias Model =
    { key : Nav.Key
    , baseApiUrl : String
    , route : Route
    , authModel : Authentication.Model
    , items : WebData (List Item)

    -- Admin properties
    , itemToDelete : Maybe Item
    , addEditItemModel : Maybe AddEditItemModel
    , editImagesForms : List EditImagesForm
    }


type alias BaseModel =
    { items : WebData (List Item)

    -- Admin properties
    , itemToDelete : Maybe Item
    , addEditItemModel : Maybe AddEditItemModel
    , editImagesForms : List EditImagesForm -- contains list of items which images

    -- are currently being edited.
    }


type alias EditImagesForm =
    { itemId : Int
    , images : Array ItemImage
    }


type alias AddEditItemModel =
    { itemForm : ItemForm
    , editItem : Maybe Item
    }


type alias ItemForm =
    { name : String
    , summary : String
    , description : String
    , bannerImageUrl : String
    , price : String
    , quantityAvailable : String
    }


type alias Item =
    { id : Int
    , name : String
    , summary : String
    , description : String
    , bannerImageUrl : Maybe String
    , price : Maybe String
    , quantityAvailable : Int
    , images : List ItemImage
    }


type alias ItemImage =
    { imageUrl : String
    , description : Maybe String
    }


init : BaseModel
init =
    { items = RemoteData.Loading
    , itemToDelete = Nothing
    , addEditItemModel = Nothing
    , editImagesForms = []
    }



-- MSG


type Msg
    = OnReceivedItems (WebData (List Item))
    | Noop
      -- Admin messages
    | ShowConfirmDeleteItemModal Item
    | CloseConfirmDeleteItemModal
    | CloseAddEditItemModal
    | StartEditItem Item
    | StartAddItem
    | SaveAddEditItemClicked
    | ItemFormInput (ItemForm -> String -> ItemForm) String
    | FinishedCreatingItem (WebData Item)
    | FinishedUpdatingItem (WebData Item)
    | FinishedDeletingItem (Result Http.Error ())
    | DeleteItemClicked
    | StartEditItemImages Item
    | CancelEditItemImages Item
    | ImageFormInput Int Int (ItemImage -> String -> ItemImage) String
    | DeleteItemImage Int Int
    | AddItemImage Int
    | SaveItemImages Int (List ItemImage)
    | FinishedUpdatingItemImages Int (WebData (List ItemImage))



-- ROUTE


type Route
    = Index
    | Admin
    | ItemDetail Int
    | About
