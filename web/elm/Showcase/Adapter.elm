module Showcase.Adapter exposing (handleUpdate, rootToShowcaseModel, showcaseToRootModel, showcaseToRootView)

import Html exposing (Html)
import Models as RootModels
import Showcase.Models
import Showcase.Update



-- MODEL ADAPTERS


rootToShowcaseModel :
    RootModels.Model
    -> Showcase.Models.Model
rootToShowcaseModel rootModel =
    let
        showcaseRoute =
            case rootModel.route of
                RootModels.Showcase r ->
                    r

                _ ->
                    Showcase.Models.Index

        -- default. probably won't get hit
    in
    { key = rootModel.key
    , authModel = rootModel.authModel
    , baseApiUrl = rootModel.baseApiUrl ++ "/showcase"
    , route = showcaseRoute
    , items = rootModel.showcaseModel.items
    , itemToDelete = rootModel.showcaseModel.itemToDelete
    , addEditItemModel = rootModel.showcaseModel.addEditItemModel
    , editImagesForms = rootModel.showcaseModel.editImagesForms
    }


showcaseToRootModel : Showcase.Models.Model -> Showcase.Models.BaseModel
showcaseToRootModel model =
    { items = model.items
    , itemToDelete = model.itemToDelete
    , addEditItemModel = model.addEditItemModel
    , editImagesForms = model.editImagesForms
    }



-- UPDATE ADAPTERS


handleUpdate : RootModels.Model -> Showcase.Models.Msg -> ( RootModels.Model, Cmd RootModels.Msg )
handleUpdate rootModel showcaseMsg =
    let
        wrappedModel =
            rootToShowcaseModel rootModel

        ( showcaseModel, cmd ) =
            Showcase.Update.update showcaseMsg wrappedModel

        newShowcaseModel =
            showcaseToRootModel showcaseModel

        newRootCmd =
            Cmd.map RootModels.OnShowcaseMsg cmd
    in
    ( { rootModel | showcaseModel = newShowcaseModel }, newRootCmd )



-- VIEW ADPTERS


showcaseToRootView :
    (Showcase.Models.Model -> { title : String, content : Html Showcase.Models.Msg })
    -> (RootModels.Model -> { title : String, content : Html RootModels.Msg })
showcaseToRootView showcaseView =
    \showcaseModel ->
        showcaseView (rootToShowcaseModel showcaseModel)
            |> convertViewOutput


convertViewOutput :
    { title : String, content : Html Showcase.Models.Msg }
    -> { title : String, content : Html RootModels.Msg }
convertViewOutput showcaseStuff =
    { title = showcaseStuff.title
    , content = Html.map RootModels.OnShowcaseMsg showcaseStuff.content
    }
