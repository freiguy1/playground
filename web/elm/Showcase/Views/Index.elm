module Showcase.Views.Index exposing (initCmd, view)

import Html exposing (Html, div, a, img, text, h1)
import Html.Attributes exposing (class, href, src, style)
import Http
import RemoteData exposing (WebData)
import Showcase.Models exposing (Model, Msg(..), Item)
import Showcase.Service exposing (fetchItems)
import Showcase.Views.Footer exposing (footerView)
import Util


initCmd : Model -> Cmd Msg
initCmd model =
    let
        isLoaded =
            model.items
                |> RemoteData.isSuccess
    in
    if isLoaded then
        Cmd.none

    else
        fetchItems model.baseApiUrl


bannerImageUrl : String
bannerImageUrl =
    "https://lh3.googleusercontent.com/ZDMVLxxryoZu4wv2R9qfabjWqnOOqH0l9wfBcwtQmpcnejUyozl-meFZiDelwe9g4qmcRa-CfDXmxbdmar9wMthFcmyS2qY8vGRfkLpancjDdQflKz6YxgjjZmL0cMt3I9YgcughmtIp4SifgyjUHmsWQE5N8yH5JaZe3b8GyyM4_BVXXN7WOtVQWTx10L1yE3AAwRL5Nn7TZ3HU7JZT_sjIkszs_0VS1XgDrqiDOTW2Rfb_zZQgen5mbkB2BrGprLGRnHad-L7-o8wY3loi-VlPqY7tK2KY4Qqje9jEC3LCi5AHpAoX8nunOJGMna1TqkC7SsZSZya5hPYA_ZlYxnvZnbbfpvjZ0KIxpisclWGF64nQaMkP7hMNKUb42G1JpkLBf-p9P_bHxgkKamtrT437vw7IE23CxcB_o0XorjG4KcG0DqXhJdpXPyFtISCLdKlXvyJQgvyZOTTp-QSN1Wr_WyaWQypp7aTUYjYvGOrOODLhY3CNt2jJnzLNrw0lRhaEiPFuInv80Hj5wjIovPJaTWOymxE1eu-yYRBuCTNyr-7SOx6EWPQDWMVNZOG_2MvBd3ve4ilNEcUgbqtipZOEhNts5FIEgvBsVVCVtmGLwbIb5IyFwodb6ufiEHWy9LUXx1bSauMvkacG_JTJHjGgLfApQLCAmQ=w1920"


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "EF Woodworking"
    , content =
        div [ style "overflow-x" "hidden" ]
            [ div
                [ class "full-screen d-flex flex-column justify-content-start justify-content-md-center align-items-end"
                , style "background-image" ("url(" ++ bannerImageUrl ++ ")")
                ]
                [ div [ class "slogan" ]
                    [ img [ src "/assets/showcase/white-ef-200px.png" ] []
                    , div [] [ text "WOODWORKING" ]
                    ]
                ]
            , maybeForSale
                (model.items
                    |> RemoteData.map
                        (\items -> items |> List.filter forSaleCheck)
                )
            , maybeItems model.items
            , footerView
            ]
    }


forSaleCheck : Item -> Bool
forSaleCheck item =
    item.quantityAvailable > 0 && Util.isJust item.price


-- adminLink : Bool -> Html Msg
-- adminLink isAdmin =
--     if isAdmin then
--         a [ href "/showcase/admin" ] [ text "Admin" ]

--     else
--         text ""


maybeForSale : WebData (List Item) -> Html Msg
maybeForSale response =
    case response of
        RemoteData.NotAsked ->
            text "this should never be seen"

        RemoteData.Loading ->
            text "Loading..."

        RemoteData.Success [] ->
            div [] []

        RemoteData.Success items ->
            div [ class "my-5" ]
                [ h1 [ class "ml-3" ] [ text "For Sale" ]
                , itemsView items
                ]

        RemoteData.Failure _ ->
            div [] []


maybeItems : WebData (List Item) -> Html Msg
maybeItems response =
    case response of
        RemoteData.NotAsked ->
            text "this should never be seen"

        RemoteData.Loading ->
            text "Loading..."

        RemoteData.Success [] ->
            div [] []

        RemoteData.Success items ->
            div [ class "my-5" ]
                [ h1 [ class "ml-3" ] [ text "All Projects" ]
                , itemsView items
                ]

        RemoteData.Failure Http.NetworkError ->
            text "Network Error"

        RemoteData.Failure _ ->
            text "Error getting items"


itemsView : List Item -> Html Msg
itemsView items =
    if List.isEmpty items then
        text "No items"

    else
        div [ class "items-list" ] (List.map itemView items)


itemView : Item -> Html Msg
itemView item =
    let
        imageUrl =
            item.bannerImageUrl
                |> Maybe.withDefault ""

        itemDetailUrl =
            "/showcase/" ++ String.fromInt item.id

        priceDisplay =
            case ( item.price, item.quantityAvailable ) of
                ( Just _, 0 ) ->
                    ""

                ( Just price, _ ) ->
                    "$" ++ price

                _ ->
                    ""
    in
    a
        [ class "item-card"
        , style "background-image" ("url(\"" ++ imageUrl ++ "\")")
        , href itemDetailUrl
        ]
        --)
        [ div [ class "item-banner tran-gray-background" ]
            [ div [ class "d-flex flex-column align-items-center flex-shrink-0" ]
                [ div [ class "item-name" ] [ text item.name ]
                , div [] [ text priceDisplay ]
                ]
            , div [ class "separator" ] []
            , div [ class "item-summary" ] [ text item.summary ]
            ]
        ]
