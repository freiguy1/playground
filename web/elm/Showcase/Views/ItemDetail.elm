module Showcase.Views.ItemDetail exposing (initCmd, view)

import Html exposing (Html, text, p, div, h1, h2, span, a, img)
import Html.Attributes exposing (class, href, src, style)
import Markdown
import RemoteData
import Showcase.Models exposing (Model, Msg(..), Item, ItemImage)
import Showcase.Service exposing (fetchItems)
import Showcase.Views.Footer exposing (footerView)
import Showcase.Views.NotFound as NotFoundView


initCmd : Model -> Cmd Msg
initCmd model =
    let
        isLoaded =
            model.items
                |> RemoteData.isSuccess
    in
    if not isLoaded then
        fetchItems model.baseApiUrl

    else
        Cmd.none


getItemFromModel : Model -> Int -> Maybe Item
getItemFromModel model id =
    case model.items of
        RemoteData.Success items ->
            items
                |> List.filter (\i -> i.id == id)
                |> List.head

        _ ->
            Nothing


getTitle : Model -> Int -> String
getTitle model id =
    case getItemFromModel model id of
        Just i ->
            i.name

        Nothing ->
            "Not Found"


view : Int -> Model -> { title : String, content : Html Msg }
view id model =
    { title = getTitle model id
    , content =
        case model.items of
            RemoteData.Success items ->
                case items |> List.filter (\i -> i.id == id) |> List.head of
                    Just i ->
                        div []
                            [ div [ class "container" ]
                                [ banner i
                                , h1 [ class "my-3 mt-md-5 ml-md-4 mb-md-0" ] [ text i.name ]
                                , div [ class "ml-md-4 font-weight-light font-italic mb-4" ] [ text i.summary ]
                                , Markdown.toHtml [ class "mb-3" ] i.description
                                , album i.images

                                -- , p [] [ text "(more images likely to go here)" ]
                                , div [ class "py-4" ] [ purchasingInfo i ]
                                ]
                            , footerView
                            ]

                    Nothing ->
                        NotFoundView.view

            RemoteData.Failure _ ->
                div [] [ text "Failed to load item" ]

            RemoteData.Loading ->
                div [] [ text "Loading data" ]

            _ ->
                div [] [ text "Something went wrong." ]
    }


purchasingInfo : Item -> Html Msg
purchasingInfo item =
    case ( item.price, item.quantityAvailable ) of
        ( Just price, 0 ) ->
            div []
                [ h2 [] [ text "Interested?" ]
                , p []
                    [ span [] [ text ("If you like this project, I can build one for you! The price is $" ++ price ++ ". Get in ") ]
                    , a [ href "mailto:contact@ethanfrei.com" ] [ text "contact with me" ]
                    , span [] [ text " and we can figure out the details." ]
                    ]
                ]

        ( Just price, quantityAvailable ) ->
            div []
                [ h2 [] [ text "Interested?" ]
                , p []
                    [ span [] [ text ("I've got " ++ String.fromInt quantityAvailable ++ " available! The price is $" ++ price ++ ". Get in ") ]
                    , a [ href "mailto:contact@ethanfrei.com" ] [ text "contact with me" ]
                    , span [] [ text " and we can figure out the details." ]
                    ]
                ]

        ( Nothing, _ ) ->
            div [] []


banner : Item -> Html Msg
banner item =
    case item.bannerImageUrl of
        Just imageUrl ->
            div []
                [ img [ src imageUrl, class "w-100" ] []
                ]

        Nothing ->
            div [] [ text "No main image yet!" ]


album : List ItemImage -> Html Msg
album images =
    div []
        (List.map albumImage images)


albumImage : ItemImage -> Html Msg
albumImage image =
    let
        descriptionView =
            case image.description of
                Just description ->
                    Markdown.toHtml [ class "p-2 px-md-5" ] description

                Nothing ->
                    div [] []
    in
    div [ class "py-3" ]
        [ div [ class "text-center" ]
            [ img [ src image.imageUrl, class "mw-100", style "max-height" "80vh" ] []
            ]
        , descriptionView
        ]
