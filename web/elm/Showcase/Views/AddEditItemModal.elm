module Showcase.Views.AddEditItemModal exposing (view)

import Bootstrap.Button as Button
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Form.Textarea as Textarea
import Bootstrap.Modal as Modal
import Html exposing (Html, p, text, span)
import Html.Attributes exposing (class, for)
import Html.Events exposing (onClick)
import Showcase.Models exposing (Model, Msg(..), AddEditItemModel, ItemForm)
import Util


view : Model -> Html Msg
view model =
    let
        configMaker =
            case model.addEditItemModel of
                Just m ->
                    guts m

                Nothing ->
                    Modal.body [] [ p [] [ text "blank modal" ] ]
    in
    Modal.config CloseAddEditItemModal
        |> configMaker
        |> Modal.view (Util.maybeToVisibility model.addEditItemModel)


guts : AddEditItemModel -> Modal.Config Msg -> Modal.Config Msg
guts model config =
    let
        headerText =
            case model.editItem of
                Just ei ->
                    "Edit " ++ ei.name

                Nothing ->
                    "Create new item"
    in
    config
        |> Modal.hideOnBackdropClick False
        |> Modal.h3 [] [ text headerText ]
        |> Modal.body [] [ formView model.itemForm ]
        |> Modal.footer []
            [ Button.button
                [ Button.outlinePrimary
                , Button.attrs [ onClick CloseAddEditItemModal ]
                ]
                [ text "Close" ]
            , Button.button
                [ Button.primary
                , Button.attrs [ onClick SaveAddEditItemClicked ]
                ]
                [ text "Save" ]
            ]


formView : ItemForm -> Html Msg
formView form =
    Form.form []
        [ Form.group []
            [ Form.label [ for "name" ] [ text "Item name" ]
            , Input.text
                [ Input.id "name"
                , Input.onInput (ItemFormInput (\f i -> { f | name = i }))
                , Input.value form.name
                ]
            ]
        , Form.group []
            [ Form.label [ for "summary" ] [ text "Summary" ]
            , Input.text
                [ Input.id "summary"
                , Input.onInput (ItemFormInput (\f i -> { f | summary = i }))
                , Input.value form.summary
                ]
            ]
        , Form.group []
            [ Form.label [ for "description" ] [ text "Description" ]
            , Textarea.textarea
                [ Textarea.id "description"
                , Textarea.rows 3
                , Textarea.onInput (ItemFormInput (\f i -> { f | description = i }))
                , Textarea.value form.description
                ]
            ]
        , Form.group []
            [ Form.label [ for "bannerImageUrl" ]
                [ span [] [ text "Banner image url" ]
                , span [ class "text-muted" ] [ text " (optional)" ]
                ]
            , Input.text
                [ Input.id "bannerImageUrl"
                , Input.onInput (ItemFormInput (\f i -> { f | bannerImageUrl = i }))
                , Input.value form.bannerImageUrl
                ]
            ]
        , Form.row []
            [ Form.col []
                [ Form.group []
                    [ Form.label [ for "quantity_available" ] [ text "Quantity" ]
                    , Input.number
                        [ Input.id "quantity_available"
                        , Input.onInput (ItemFormInput (\f i -> { f | quantityAvailable = i }))
                        , Input.value form.quantityAvailable
                        ]
                    ]
                ]
            , Form.col []
                [ Form.group []
                    [ Form.label [ for "price" ]
                        [ span [] [ text "Price" ]
                        , span [ class "text-muted" ] [ text " (optional)" ]
                        ]
                    , Input.text
                        [ Input.id "price"
                        , Input.onInput (ItemFormInput (\f i -> { f | price = i }))
                        , Input.value form.price
                        ]
                    ]
                ]
            ]
        ]
