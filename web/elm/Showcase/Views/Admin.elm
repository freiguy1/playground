module Showcase.Views.Admin exposing (initCmd, view)

import Array
import Bootstrap.Button as Button
import Bootstrap.Card as Card
import Bootstrap.Card.Block as CardBlock
import Bootstrap.Form as Form
import Bootstrap.Form.Input as Input
import Bootstrap.Form.Textarea as Textarea
import Bootstrap.Modal as Modal
import Html exposing (Html, a, div, h2, h6, img, p, span, text)
import Html.Attributes exposing (class, for, href, src, style)
import Html.Events exposing (onClick)
import RemoteData
import Showcase.Models exposing (Item, ItemImage, Model, Msg(..))
import Showcase.Service exposing (fetchItems)
import Showcase.Views.AddEditItemModal as AddEditItemModal
import Util


initCmd : Model -> Cmd Msg
initCmd model =
    let
        isLoaded =
            model.items
                |> RemoteData.isSuccess
    in
    if isLoaded then
        Cmd.none

    else
        fetchItems model.baseApiUrl


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "EF Woodworking - Admin"
    , content =
        div [ class "container" ]
            [ h2 [] [ text "Showcase admin page" ]
            , a
                [ href "/showcase", class "m-1" ]
                [ text "Public Page" ]
            , a
                [ href "/", class "m-1" ]
                [ text "Playground" ]
            , Button.button
                [ Button.primary
                , Button.attrs
                    [ onClick StartAddItem
                    , class "m-1"
                    ]
                ]
                [ text "Add Item" ]
            , maybeItems model
            , deleteModal model
            , AddEditItemModal.view model
            ]
    }


maybeItems :
    Model
    -> Html Msg -- RemoteData.WebData (List Item) -> Html Msg
maybeItems model =
    case model.items of
        RemoteData.Success items ->
            itemsView items model

        RemoteData.Failure _ ->
            text "Failed to load item data"

        -- could send e to method to be more specific
        _ ->
            text "Loading..."


itemsView : List Item -> Model -> Html Msg
itemsView items model =
    if List.isEmpty items then
        text "No items"

    else
        div [] (List.map (itemView model) items)


itemView : Model -> Item -> Html Msg
itemView model item =
    let
        bannerDiv =
            case item.bannerImageUrl of
                Just url ->
                    div
                        [ style "background-image" ("url(\"" ++ url ++ "\")")
                        , style "background-size" "cover"
                        , style "background-repeat" "no-repeat"
                        , style "background-position" "center"
                        , style "flex-shrink" "0"
                        , style "width" "200px"
                        , class "mr-3"
                        ]
                        []

                Nothing ->
                    div [] []

        itemDetailUrl =
            "/showcase/" ++ String.fromInt item.id

        link =
            a
                [ href itemDetailUrl, class "ml-1" ]
                [ text "Link" ]

        title =
            case item.price of
                Just price ->
                    [ text item.name
                    , span [ class "text-muted" ] [ text (" $" ++ price) ]
                    , link
                    ]

                Nothing ->
                    [ text item.name, link ]
    in
    Card.config [ Card.attrs [ class "mb-3" ] ]
        |> Card.header [] title
        |> Card.block []
            [ CardBlock.custom <|
                div []
                    [ div [ class "d-flex flex-row" ]
                        [ bannerDiv
                        , div []
                            [ h6
                                [ class "card-subtitle text-muted mb-2" ]
                                [ text item.summary ]
                            , p [] [ text item.description ]
                            , Button.button
                                [ Button.primary
                                , Button.attrs
                                    [ onClick (StartEditItem item)
                                    , class "m-1"
                                    ]
                                ]
                                [ text "Edit" ]
                            , editAlbumButton model item
                            , Button.button
                                [ Button.danger
                                , Button.attrs
                                    [ onClick (ShowConfirmDeleteItemModal item)
                                    , class "m-1"
                                    ]
                                ]
                                [ text "Delete" ]
                            ]
                        ]
                    , editAlbumForm model item
                    ]
            ]
        |> Card.view


editAlbumButton : Model -> Item -> Html Msg
editAlbumButton model item =
    if List.any (\f -> f.itemId == item.id) model.editImagesForms then
        span [] []

    else
        Button.button
            [ Button.attrs
                [ onClick (StartEditItemImages item)
                , class "m-1"
                ]
            ]
            [ text "Album" ]


editAlbumForm : Model -> Item -> Html Msg
editAlbumForm model item =
    case model.editImagesForms |> List.filter (\f -> f.itemId == item.id) |> List.head of
        Just form ->
            div [ class "py-3" ]
                [ div []
                    (List.map
                        (\( index, image ) -> editAlbumFormImage item.id index image)
                        (Array.toIndexedList form.images)
                    )
                , div [ class "d-flex justify-content-center" ]
                    [ Button.button
                        [ Button.success
                        , Button.attrs
                            [ class "m-1", onClick (AddItemImage item.id) ]
                        ]
                        [ text "Add Image" ]
                    , Button.button
                        [ Button.attrs
                            [ class "m-1", onClick (CancelEditItemImages item) ]
                        ]
                        [ text "Cancel" ]
                    , Button.button
                        [ Button.primary
                        , Button.attrs
                            [ class "m-1", onClick (SaveItemImages item.id (Array.toList form.images)) ]
                        ]
                        [ text "Save" ]
                    ]
                ]

        Nothing ->
            -- not editing album currently
            div [] []


editAlbumFormImage : Int -> Int -> ItemImage -> Html Msg
editAlbumFormImage itemId imageIndex image =
    div [ class "row" ]
        [ div [ class "col-2 d-flex justify-content-center align-items-center" ]
            [ img [ class "mw-100", src image.imageUrl ] [] ]
        , div [ class "col" ]
            [ Form.group []
                [ Form.label [ for "image-url" ] [ text "Image url" ]
                , Input.text
                    [ Input.id "image-url"
                    , Input.onInput (ImageFormInput itemId imageIndex (\f i -> { f | imageUrl = i }))
                    , Input.value image.imageUrl
                    ]
                ]
            , Form.group []
                [ Form.label [ for "description" ] [ text "Description" ]
                , Textarea.textarea
                    [ Textarea.id "description"
                    , Textarea.rows 3
                    , Textarea.onInput (ImageFormInput itemId imageIndex (\f i -> { f | description = Just i }))
                    , Textarea.value (Maybe.withDefault "" image.description)
                    ]
                ]
            ]
        , div [ class "col-1 d-flex justify-content-center align-items-center" ]
            [ Button.button
                [ Button.danger
                , Button.attrs
                    [ onClick (DeleteItemImage itemId imageIndex) ]
                ]
                [ text "Delete" ]
            ]
        ]


deleteModal : Model -> Html Msg
deleteModal model =
    let
        configMaker =
            case model.itemToDelete of
                Just i ->
                    deleteModalGuts i

                Nothing ->
                    -- shouldn't ever happen
                    Modal.body [] [ p [] [ text "blank modal" ] ]
    in
    Modal.config CloseConfirmDeleteItemModal
        |> configMaker
        |> Modal.view (Util.maybeToVisibility model.itemToDelete)


deleteModalGuts : Item -> Modal.Config Msg -> Modal.Config Msg
deleteModalGuts item config =
    config
        |> Modal.small
        |> Modal.hideOnBackdropClick True
        |> Modal.h3 [] [ text ("Delete " ++ item.name ++ "?") ]
        |> Modal.body [] [ p [] [ text "Are you sure?" ] ]
        |> Modal.footer []
            [ Button.button
                [ Button.outlinePrimary
                , Button.attrs [ onClick CloseConfirmDeleteItemModal ]
                ]
                [ text "Cancel" ]
            , Button.button
                [ Button.primary
                , Button.attrs [ onClick DeleteItemClicked ]
                ]
                [ text "Yes, delete" ]
            ]
