module Showcase.Views.About exposing (view)

import Html exposing (Html, div, h1, text)
import Html.Attributes exposing (class, style)
import Markdown
import Showcase.Models exposing (Model, Msg(..))
import Showcase.Views.Footer exposing (footerView)


bannerImageUrl : String
bannerImageUrl =
    "https://lh3.googleusercontent.com/jxVwZNUeuP0sHP79IOqHFP8qniEJylrMAVwz9otJNtGjiAaEAnPvnre1lsGYd7Dlfmgs3075-7TGJkaq9p8iVzjT_FcPQiE2Xi8tCUOpRgs8C-N2O6kaE03S6sJ9cXhs1tw6RVezFCoPV-FQVkF0gYowjgp4umd6CkSh_DcK3flr_Z3VZINcAuTdev1WUOe8N3KtyTJdI5rWneMUxuQgBZhJJRV3QEdEJtgyVcpYZKHx-JzL4uxhjeARBOCwEAlPanSyfVi7r6e46LvtD9oWMtHwPPt3-gCBShvdxs-W5IklNfAhWfiluF4D2bnCiHXOEH26G9aTyHCjfeXv8UeIzEfrlhTYEcnN94SEXRBhcy4zgA7C9TmIP-IhCfUYnEi7RZ_Lp4nFrQ03UBCO3xs2UOH0VgNWQqzvB-OXOfL52TNOydB-nDxFm85lGOVYKk8yeGAZhQyPZgb_FFk5VlHWhcrWXDKzq5oLxczdQaoBvoRa7OBLuGX3FMHMTfv_A3NrP30JgR5D26IEByCZ5trVOQyKwnKG43mSTlMMx88OAVpWGkjTMD0NvpiG9L2AkMmsDFPuiemKSP7R0rZjKL3BGesu2lAe5hCoDAhnxT1sr-iunoDFUr2RMRKTQZPh7VGa6LwlPnPhm7iL1SdrSqjuBP29fPBfSJ-d0w=w1900"


theWoodworkerMarkdown : String
theWoodworkerMarkdown =
    """Hello! My name is Ethan Frei.  Welcome to this custom built portfolio website
for my woodworking hobby. By day I'm a programmer, and by night I'm anything from jerky-maker
to amateur guitarist.

Woodworking has been a hobby of mine for years. Most of the things I make
are designed by myself. My designs usually prioritize utility over creative beauty, however the
quality workmanship and materials allow beauty to arise organically. My technique is based heavily
around old fashioned joinery (as opposed to screws and nails) and the use of real wood as (opposed
to manufactured sheet materials). These techniques make beautiful pieces that are also very strong.
My hope is that the build quality and strength of my build and design technique will allow for items
to be passed down for generations. Using real wood and doing a lot of joinery by hand means no two
pieces will be exactly the same.

Check out my social links in the footer, and here is a link to my
[blog](http://ethanfrei.com) which has a mix of technical and woodworking posts."""


theIdeaMarkdown : String
theIdeaMarkdown =
    """**EF Woodworking** is just a way for me to brand (physically and trademark-ally (?)) my creations.
It is not a business or entity on its own, and I just do woodworking here and there during free time.
Could this ever be a full time gig? Yeah I think so; so far my commissions net me about a quarter
of the hourly rate compared to programming. With bigger and badder tools, and improving my workflow
to act more like a production shop, I could get that number up. However, I'm afraid to sacrifice
the fun of woodworking if my shop becomes all about the bottom line and maximizing the completed product count.
To me, this doesn't sound like a hobby anymore.

So for now, I'll continue making personal projects, Christmas presents and doing small orders for
people when I have the time! Be sure to [contact me](mailto:contact@ethanfrei.com) if you like
what you see and would like something made."""


theWebsiteMarkdown : String
theWebsiteMarkdown =
    """This website you're viewing is made from scratch by me. I am not using any fancy CMS like
WordPress, Drupal, Squarespace, or Wix. I first started this site as an experiment with some new
technologies and so I could show it off in my software portfolio. I'm able to manage the site completely
within the site by visiting the administration portal. There, I can add new items, list items for sale, and
manage all the verbiage and pictures. I took all the pictures used on the site with some help from
my wife. I've attempted to make this whole site mobile-friendly and desktop-friendly. Say something if
you don't find that's the case. If you're interested in the technologies I used, read on!

The front end for this app is built entirely on [Elm](http://elm-lang.org/). Elm is a unique front-end
language that is functional and statically typed which leads to no runtime exceptions! Using it has been
great, but also challenging (in a good way). Using a language like this often seems to max out my brain's
capacity, but I really enjoy that because everytime I hit that threshold, I believe that capacity increases.

The back end is built in [Rust](https://rust-lang.org). Rust is a systems language (like C++ or Go) that
contains a great type system and eliminates common system language errors like segmentation faults or double
frees. I like Rust for a lot of the same reasons as Elm. Both languages have extremely smart compilers and
get rid of whole classes of runtime errors. Rust also stretches my programming intellect in the same way
Elm does. I think this stack is great if you're up for investing more in the programming part of a project
so you don't need to invest as much time in bug fixing and patching.

For data persistence I'm using [PostgreSQL](https://www.postgresql.org/). The app is being hosted
in [Heroku](https://heroku.com). For authentication, I'm using [Auth0](https://auth0.com). I made a
[blog post](http://ethanfrei.com/posts/rust-with-rocket-elm-running-on-heroku.html) on setting up
this stack if you are interested."""


view : Model -> { title : String, content : Html Msg }
view _ =
    { title = "EF Woodworking - About"
    , content =
        div []
            [ banner
            , div [ class "container" ]
                [ theWoodworker
                ]
            , footerView
            ]
    }


theWoodworker : Html Msg
theWoodworker =
    div [ class "mb-5" ]
        [ h1 [ class "mt-5" ] [ text "The woodworker" ]
        , Markdown.toHtml [] theWoodworkerMarkdown
        , h1 [ class "mt-5" ] [ text "The idea" ]
        , Markdown.toHtml [] theIdeaMarkdown
        , h1 [ class "mt-5" ] [ text "The website" ]
        , Markdown.toHtml [] theWebsiteMarkdown
        ]


banner : Html Msg
banner =
    div
        [ style "background-image" ("url(" ++ bannerImageUrl ++ ")")
        , class "small-banner"
        ]
        []
