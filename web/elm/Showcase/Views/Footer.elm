module Showcase.Views.Footer exposing (footerView)

import Html exposing (Html, div, text, a, footer, img, i)
import Html.Attributes exposing (class, href, src)
import Showcase.Models exposing (Msg(..))


footerView : Html Msg
footerView =
    footer [ class "py-5 bg-dark text-white " ]
        [ div [ class "d-flex flex-column flex-md-row justify-content-center align-items-center mb-5" ]
            [ div [ class "d-flex align-items-center" ]
                [ img [ src "/assets/showcase/white-ef-30px.png" ] []
                , div [ class "font-mist ml-2 font-size-2" ] [ text "WOODWORKING" ]
                ]
            , div [ class "big-white-separator d-none d-md-block" ] []
            , div [ class "d-flex flex-column justify-content-center" ]
                [ a
                    [ href "/showcase", class "text-light-blue mb-1" ]
                    [ text "Home" ]
                , a
                    [ href "/showcase/about", class "text-light-blue mb-1" ]
                    [ text "About" ]
                ]
            ]
        , div [ class "d-flex justify-content-center" ]
            [ a [ href "https://www.youtube.com/user/ethanfrei", class "px-4" ]
                [ i [ class "fab fa-youtube text-white font-size-4" ] []
                ]
            , a [ href "https://www.facebook.com/ethan.d.frei", class "px-4" ]
                [ i [ class "fab fa-facebook text-white font-size-4" ] []
                ]
            , a [ href "https://www.twitter.com/e_frei", class "px-4" ]
                [ i [ class "fab fa-twitter text-white font-size-4" ] []
                ]
            ]
        ]
