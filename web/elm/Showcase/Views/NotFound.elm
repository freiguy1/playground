module Showcase.Views.NotFound exposing (view)

import Html exposing (Html, div, h3, a, text)
import Html.Attributes exposing (class, style, href)
import Showcase.Models exposing (Msg(..))


backgroundImageUrl : String
backgroundImageUrl =
    "https://lh3.googleusercontent.com/JEyCsDYwgPHPf4brI6FojOf-tAQZ5w21Dg8PKWOy1wpZcxrVK8Qr3hBqDgv8AvahS9iMvAC4Yi0fLb5dSIzHp6oni9XVkYB9mIXMbLZZwXPekJraQY3qwMPSyo-9xJdMvrXiUHHBA9iJe2cGKtFpVGFCjsP5C5b2z76JppHgfxN5asl8ZxQKKEfmtZYEJd6-D_dKEWq5rKFPS--vvZKKUGf-k71YFjAbZZdbDANnKQoLds0m3Ke7utqeY6h5bZDgovij3Y0Y3Z90K477LBsQMsnDqKE_qD4scnCZclfEOAVdZlJygWVDrdO8WGxxvrRWlYed1-eHo0ZnhPuJnoxxxNrpvcrGszvmccMAsNQAiiV2ZkPMYge8hEquswlGlvcoNKJ-damJ5nVmzqsk74ImaVHIZQ1Tfex8wXUfa8fzkJwO3oYzg9yjC9D8GiQAwOh5ISxqKb2jTI83w855h8JWRtvoQkJZffc_-EZglYVoFtpgFcWBfQ2hsDQR70s2R74z53rWZoK5eaZSrA9Pa7BVxCsUdy_l2O7aQj56bNPZr1jzLtFNJBnAtFAr1-Obs3oTiLijCog1scxHtHNm9OeJ9pVZyHVH2Q9lAvxdBz9YTSy7RhMAXJ8MMyZY8J4Ky5x2sYo1OMQ9V40D0ZPHECUtAe231Uln1ZfrVQ=w1920"


view : Html Msg
view =
    div
        [ class "full-screen d-flex justify-content-center align-items-start"
        , style "background-image" ("url(" ++ backgroundImageUrl ++ ")")
        ]
        [ div [ class "mt-5 p-5 tran-gray-background text-center" ]
            [ h3 [ class "text-white" ] [ text "Page not found" ]
            , a
                [ href "/showcase", class "text-light-blue" ]
                [ text "Back to woodworking showcase" ]
            ]
        ]
