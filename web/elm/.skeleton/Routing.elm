module Skeleton.Routing exposing (getCmdFromRouteAndModel, getViewFromRoute, matchers)

import Html exposing (Html)
import Skeleton.Index as IndexView
import Skeleton.Models exposing (..)
import Url.Parser exposing (Parser, int, map, oneOf, s, top)


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map Index top
        ]


getCmdFromRouteAndModel : Route -> Model -> Cmd Msg
getCmdFromRouteAndModel route model =
    case route of
        Index ->
            IndexView.initCmd model


getViewFromRoute : Route -> (Model -> { title : String, content : Html Msg })
getViewFromRoute route =
    case route of
        Index ->
            IndexView.view
