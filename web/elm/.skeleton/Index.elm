module Skeleton.Index exposing (initCmd, view)

import Html exposing (..)
import Skeleton.Models exposing (..)


initCmd : Model -> Cmd Msg
initCmd model =
    Cmd.none


view : Model -> { title : String, content : Html Msg }
view model =
    { title = "Activity Skeleton"
    , content = div [] [ text "Hello from skeleton" ]
    }
