module Skeleton.Models exposing (BaseModel, Model, Msg(..), Route(..), init)

import Authentication
import Browser.Navigation as Nav
import Time exposing (Posix)



-- MODEL
-- contains everything in BaseModel plus global things


type alias Model =
    { key : Nav.Key
    , baseApiUrl : String
    , route : Route
    , authModel : Authentication.Model
    , dates : Maybe (List Posix)
    }


type alias BaseModel =
    { dates : Maybe (List Posix)
    }


init : BaseModel
init =
    { dates = Nothing
    }



-- MSG


type Msg
    = Noop



-- ROUTE


type Route
    = Index
