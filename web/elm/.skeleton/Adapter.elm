module Skeleton.Adapter exposing (handleUpdate, rootToTrackerModel, skeletonToRootModel, skeletonToRootView)

import Authentication
import Html exposing (Html)
import Models as RootModels
import Skeleton.Models
import Skeleton.Update
import Url exposing (..)



-- MODEL ADAPTERS


rootToSkeletonModel :
    RootModels.Model
    -> Skeleton.Models.Model
rootToSkeletonModel rootModel =
    let
        skeletonRoute =
            case rootModel.route of
                RootModels.Skeleton r ->
                    r

                _ ->
                    -- default. probably won't get hit
                    Skeleton.Models.Index
    in
    { key = rootModel.key
    , authModel = rootModel.authModel
    , baseApiUrl = rootModel.baseApiUrl ++ "/skeleton"
    , route = skeletonRoute
    , dates = rootModel.skeletonModel.dates
    }


skeletonToRootModel : Skeleton.Models.Model -> Tracker.Models.BaseModel
skeletonToRootModel model =
    { dates = model.dates
    }



-- UPDATE ADAPTERS


handleUpdate : RootModels.Model -> Skeleton.Models.Msg -> ( RootModels.Model, Cmd RootModels.Msg )
handleUpdate rootModel skeletonMsg =
    let
        skeletonRoute =
            case rootModel.route of
                RootModels.Skeleton r ->
                    r

                _ ->
                    -- default. probably won't get hit
                    Skeleton.Models.Index

        wrappedModel =
            rootToSkeletonModel rootModel

        ( skeletonModel, cmd ) =
            Skeleton.Update.update skeletonMsg wrappedModel

        newSkeletonModel =
            skeletonToRootModel skeletonModel

        newRootCmd =
            Cmd.map RootModels.OnSkeletonMsg cmd
    in
    ( { rootModel | skeletonModel = newSkeletonModel }, newRootCmd )



-- VIEW ADPTERS


skeletonToRootView :
    (Skeleton.Models.Model -> { title : String, content : Html Tracker.Models.Msg })
    -> (RootModels.Model -> { title : String, content : Html RootModels.Msg })
skeletonToRootView skeletonView =
    \skeletonModel ->
        skeletonView (rootToSkeletonModel skeletonModel)
            |> convertViewOutput


convertViewOutput :
    { title : String, content : Html Skeleton.Models.Msg }
    -> { title : String, content : Html RootModels.Msg }
convertViewOutput skeletonStuff =
    { title = skeletonStuff.title
    , content = Html.map RootModels.OnSkeletonMsg skeletonStuff.content
    }
