module Skeleton.Update exposing (update)

import Array
import Browser.Dom exposing (setViewport)
import Browser.Navigation exposing (pushUrl)
import RemoteData exposing (WebData)
import Skeleton.Models exposing (..)
import Task


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Noop ->
            ( model, Cmd.none )
