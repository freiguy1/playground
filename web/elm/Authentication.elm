module Authentication exposing
    ( Model
    , Msg(..)
    , fetchIsAdmin
    , handleAuthResult
    , init
    , isLoggedIn
    , tryGetUserProfile
    , update
    )

import Auth0
import Http
import Json.Decode as Decode
import RemoteData


type alias Model =
    { state : Auth0.AuthenticationState
    , lastError : Maybe Auth0.AuthenticationError
    , authorize : Auth0.Options -> Cmd Msg
    , logOut : () -> Cmd Msg
    , isAdmin : Bool
    }


init : (Auth0.Options -> Cmd Msg) -> (() -> Cmd Msg) -> Maybe Auth0.LoggedInUser -> Model
init authorize logOut initialData =
    { state =
        case initialData of
            Just user ->
                Auth0.LoggedIn user

            Nothing ->
                Auth0.LoggedOut
    , lastError = Nothing
    , authorize = authorize
    , logOut = logOut
    , isAdmin = False
    }


type Msg
    = AuthenticationResult Auth0.AuthenticationResult
    | ShowLogIn
    | LogOut
    | OnReceivedIsAdmin (RemoteData.WebData Bool)


update : String -> Msg -> Model -> ( Model, Cmd Msg )
update baseApiUrl msg model =
    case msg of
        AuthenticationResult result ->
            let
                ( newState, error ) =
                    case result of
                        Ok user ->
                            ( Auth0.LoggedIn user, Nothing )

                        Err err ->
                            ( model.state, Just err )

                newModel =
                    { model | state = newState, lastError = error }
            in
            ( newModel, fetchIsAdmin baseApiUrl newModel )

        ShowLogIn ->
            ( model, model.authorize Auth0.defaultOpts )

        LogOut ->
            ( { model | state = Auth0.LoggedOut, isAdmin = False }, model.logOut () )

        OnReceivedIsAdmin result ->
            case result of
                RemoteData.Success isAdmin ->
                    ( { model | isAdmin = isAdmin }, Cmd.none )

                _ ->
                    ( { model | isAdmin = False }, Cmd.none )


fetchIsAdmin : String -> Model -> Cmd Msg
fetchIsAdmin baseApiUrl model =
    let
        headers =
            case model.state of
                Auth0.LoggedIn user ->
                    [ Http.header "Authorization" ("Bearer " ++ user.token) ]

                _ ->
                    []

        request =
            { method = "GET"
            , headers = headers
            , url = baseApiUrl ++ "/is_admin"
            , body = Http.emptyBody
            , expect = Http.expectJson (RemoteData.fromResult >> OnReceivedIsAdmin) Decode.bool
            , timeout = Nothing
            , tracker = Nothing
            }

        cmd =
            Http.request request
    in
    case model.state of
        Auth0.LoggedIn _ ->
            cmd

        _ ->
            Cmd.none


handleAuthResult : Auth0.RawAuthenticationResult -> Msg
handleAuthResult =
    Auth0.mapResult >> AuthenticationResult


tryGetUserProfile : Model -> Maybe Auth0.UserProfile
tryGetUserProfile model =
    case model.state of
        Auth0.LoggedIn user ->
            Just user.profile

        Auth0.LoggedOut ->
            Nothing


isLoggedIn : Model -> Bool
isLoggedIn model =
    case model.state of
        Auth0.LoggedIn _ ->
            True

        Auth0.LoggedOut ->
            False
