port module Main exposing (StartupInfo, auth0authResult, auth0authorize, auth0logout, init, main, subscriptions)

import Auth0
import Authentication
import Browser
import Browser.Navigation as Nav
import Gutenberg.Models
import Models exposing (Model, Msg(..), Route(..))
import RemoteData
import Routing
import Showcase.Models
import Update exposing (update)
import Url exposing (Url)


type alias StartupInfo =
    { authStuff : Maybe Auth0.LoggedInUser
    , baseApiUrl : String
    }


main : Program StartupInfo Model Msg
main =
    Browser.application
        { init = init
        , view = Routing.rootView
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = OnUrlChange
        , onUrlRequest = OnUrlRequest
        }


init : StartupInfo -> Url -> Nav.Key -> ( Model, Cmd Msg )
init startupInfo location key =
    let
        authModel =
            Authentication.init auth0authorize auth0logout startupInfo.authStuff

        blankModel =
            { route = Index -- this route is just temporary; will get replaced
            , authModel = authModel
            , baseApiUrl = startupInfo.baseApiUrl
            , key = key
            , showcaseModel = Showcase.Models.init
            , gutenbergModel = Gutenberg.Models.init
            , mcModel =
                { status = RemoteData.Loading
                , errorMessage = Nothing
                , serverChangingStatus = False
                }
            }

        ( route, cmd ) =
            Routing.getRouteAndCmdFromLocation location blankModel

        initialModel =
            { blankModel | route = route }

        cmdList =
            [ cmd
            , Authentication.fetchIsAdmin startupInfo.baseApiUrl initialModel.authModel
                |> Cmd.map AuthenticationMsg
            ]
    in
    ( initialModel, Cmd.batch cmdList )



-- PORTS


port auth0authorize : Auth0.Options -> Cmd msg


port auth0authResult : (Auth0.RawAuthenticationResult -> msg) -> Sub msg


port auth0logout : () -> Cmd msg



-- SUBSCRIPTIONS


subscriptions : a -> Sub Msg
subscriptions _ =
    auth0authResult (Authentication.handleAuthResult >> AuthenticationMsg)
