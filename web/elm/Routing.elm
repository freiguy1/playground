module Routing exposing (getRouteAndCmdFromLocation, rootView)

import Browser
import Gutenberg.Adapter
import Gutenberg.Routing
import Html exposing (Html, div)
import Html.Attributes exposing (class)
import Models exposing (Model, Msg(..), Route(..))
import Service
import Showcase.Adapter
import Showcase.Routing
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s, top)
import View


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map Models.Index top
        , map Models.Mc (s "mc")
        , map Models.Showcase (s "showcase" </> Showcase.Routing.matchers)
        , map Models.Gutenberg (s "gutenberg" </> Gutenberg.Routing.matchers)
        ]


getRouteAndCmdFromLocation : Url -> Model -> ( Route, Cmd Msg )
getRouteAndCmdFromLocation location model =
    let
        routeOption =
            parse matchers location
    in
    case routeOption of
        Just (Models.Showcase subRoute) ->
            let
                route =
                    Models.Showcase subRoute

                showcaseModel =
                    Showcase.Adapter.rootToShowcaseModel model

                showcaseCmd =
                    Showcase.Routing.getCmdFromRouteAndModel subRoute showcaseModel

                cmd =
                    showcaseCmd |> Cmd.map OnShowcaseMsg
            in
            ( route, cmd )

        Just (Models.Gutenberg subRoute) ->
            let
                route =
                    Models.Gutenberg subRoute

                gutenbergModel =
                    Gutenberg.Adapter.rootToGutenbergModel model

                gutenbergCmd =
                    Gutenberg.Routing.getCmdFromRouteAndModel subRoute gutenbergModel

                cmd =
                    gutenbergCmd |> Cmd.map OnGutenbergMsg
            in
            ( route, cmd )

        Just Models.Mc ->
            ( Models.Mc, Service.fetchMcStatus model.baseApiUrl )

        Just route ->
            ( route, Cmd.none )

        Nothing ->
            ( Models.NotFound, Cmd.none )


getViewFromRoute : Route -> (Model -> { title : String, content : Html Msg })
getViewFromRoute route =
    case route of
        Models.Index ->
            View.indexView

        Models.Showcase subRoute ->
            \model ->
                let
                    viewFunc =
                        Showcase.Routing.getViewFromRoute subRoute
                            |> Showcase.Adapter.showcaseToRootView

                    { title, content } =
                        viewFunc model

                    wrappedContent =
                        div [ class "showcase-container" ] [ content ]
                in
                { title = title, content = wrappedContent }

        Models.Gutenberg subRoute ->
            \model ->
                let
                    viewFunc =
                        Gutenberg.Routing.getViewFromRoute subRoute
                            |> Gutenberg.Adapter.gutenbergToRootView

                    { title, content } =
                        viewFunc model

                    wrappedContent =
                        div [ class "gutenberg-container" ] [ content ]
                in
                { title = title, content = wrappedContent }

        Models.NotFound ->
            \_ -> View.notFoundView

        Models.Mc ->
            \model -> View.mcView model.mcModel


rootView : Model -> Browser.Document Msg
rootView model =
    let
        viewFunc =
            getViewFromRoute model.route

        { title, content } =
            viewFunc model
    in
    { title = title
    , body = [ content ]
    }
