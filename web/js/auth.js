
export function renewTokenLoop(webAuth, elmApp) {
  webAuth.checkSession({},
    function(err, authResult) {
      if (err) {
        console.log(err);
      } else {
        setTimeout(function() {
          renewTokenLoop(webAuth, elmApp);
        }, (authResult.expiresIn - 10) * 1000);
        var token = authResult.accessToken;
        var result = { err: null, ok: null };
        result.ok = {
          profile: JSON.parse(localStorage.getItem('profile')),
          token: token
        };
        localStorage.setItem('token', token);
        elmApp.ports.auth0authResult.send(result);
      }
    }
  );
}

export function getAuthData () {
  var storedProfile = localStorage.getItem('profile');
  var storedToken = localStorage.getItem('token');
  return storedProfile && storedToken ? { profile: JSON.parse(storedProfile), token: storedToken } : null;
}

export function init (elmApp) {
  var webAuth = new auth0.WebAuth({
    domain: AUTH0_DOMAIN,
    clientID: AUTH0_CLIENT_ID,
    scope: 'email profile openid',
    responseType: 'token',
    redirectUri: AUTH0_REDIRECT_URI,
    audience: AUTH0_AUDIENCE,
  });

  // Auth0 authorize subscription
  elmApp.ports.auth0authorize.subscribe(function(opts) {
    webAuth.authorize();
  });

  // Log out of Auth0 subscription
  elmApp.ports.auth0logout.subscribe(function(opts) {
    localStorage.removeItem('profile');
    localStorage.removeItem('token');
  });

  // Watching for hash after redirect
  webAuth.parseHash({ hash: window.location.hash }, function(err, authResult) {
    if (err) {
      return console.error(err);
    }
    if (authResult) {
      webAuth.client.userInfo(authResult.accessToken, function(err, profile) {
        var result = { err: null, ok: null };
        var token = authResult.accessToken;

        if (err) {
          result.err = err.details;
          // Ensure that optional fields are on the object
          result.err.name = result.err.name ? result.err.name : null;
          result.err.code = result.err.code ? result.err.code : null;
          result.err.statusCode = result.err.statusCode ? result.err.statusCode : null;
        }
        if (profile) {
          result.ok = { profile: profile, token: token };
          localStorage.setItem('profile', JSON.stringify(profile));
          localStorage.setItem('token', token);

          setTimeout(function() {
            renewTokenLoop(webAuth, elmApp);
          }, (authResult.expiresIn - 10) * 1000);
        }
        elmApp.ports.auth0authResult.send(result);
      });
      window.location.hash = '';
    }
  });

  return webAuth;
}
