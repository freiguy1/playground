'use strict';

import "../scss/styles.scss"
import Elm from '../elm/Main'
import { init, getAuthData, renewTokenLoop } from './auth';

var authData = getAuthData();

var trackerData = localStorage.getItem('trackerData');
if (trackerData === null)  trackerData = []
else trackerData = JSON.parse(trackerData)

var app = Elm.Elm.Main.init({
    flags: { authStuff: authData, baseApiUrl: BASE_API_URL, trackerData: trackerData }
});

var auth0 = init(app);

if (authData !== null) renewTokenLoop(auth0, app);

