use diesel::pg::PgConnection;
use diesel::r2d2::{Builder, ConnectionManager, Pool};
use dotenv::dotenv;
use rocket::http::Method;
use rocket_cors::{AllowedOrigins, Cors, CorsOptions};
use std::env;

type PgPool = Pool<ConnectionManager<PgConnection>>;

// The URL to the database, set via the `DATABASE_URL` environment variable.
static DATABASE_UR_ENV: &'static str = "DATABASE_URL";

/// Initializes a database pool.
pub fn init_db_pool() -> PgPool {
    dotenv().ok();
    let database_url = env::var(DATABASE_UR_ENV).expect("DATABASE_URL must be set");
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    Builder::new()
        .max_size(4)
        .build(manager)
        .expect("Could not initialize db pool")
}

pub fn init_cors() -> Cors {
    let allowed_origins =
        AllowedOrigins::some_exact(&["http://localhost:3000", "http://playground.ethanfrei.com"]);

    CorsOptions {
        allowed_origins: allowed_origins,
        allowed_methods: vec![Method::Get, Method::Post, Method::Delete, Method::Put]
            .into_iter()
            .map(From::from)
            .collect(),
        allow_credentials: true,
        ..Default::default()
    }
    .to_cors()
    .expect("Unable to initialize cors settings")
}
