#![feature(decl_macro, proc_macro_hygiene)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;
extern crate frank_jwt as jwt;

mod api;
mod auth;
mod guards;
mod init;
mod schema;

use dotenv::dotenv;
use rocket::response::NamedFile;
use std::path::{Path, PathBuf};

static ENV_NAME_ENV: &'static str = "ENV_NAME";

fn main() {
    let mut rocket = rocket::ignite();
    rocket = api::init_routes(rocket);
    rocket = rocket
        .manage(init::init_db_pool())
        .mount("/assets", routes![assets])
        .mount("/", routes![spa_path, spa_empty]);

    dotenv().ok();
    // let is_dev = env::var(ENV_NAME_ENV).map(|e| e == "dev").unwrap_or(false);
    // if is_dev {
    //     rocket = rocket.attach(init::init_cors());
    // }
    rocket = rocket.attach(init::init_cors());

    rocket.launch();
}

#[get("/<path..>")]
fn assets(path: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/assets").join(path)).ok()
}

#[get("/")]
fn spa_empty() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/index.html")).ok()
}

#[get("/<_path..>", rank = 4)]
fn spa_path(_path: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/index.html")).ok()
}
