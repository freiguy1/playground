table! {
    showcase_item_images (id) {
        id -> Int4,
        image_url -> Varchar,
        description -> Nullable<Varchar>,
        item_id -> Int4,
        sequence -> Int4,
    }
}

table! {
    showcase_items (id) {
        id -> Int4,
        name -> Varchar,
        summary -> Varchar,
        description -> Varchar,
        banner_image_url -> Nullable<Varchar>,
        price -> Nullable<Numeric>,
        quantity_available -> Int4,
    }
}

joinable!(showcase_item_images -> showcase_items (item_id));

allow_tables_to_appear_in_same_query!(
    showcase_item_images,
    showcase_items,
);
