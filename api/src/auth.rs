use crate::jwt::{decode, Algorithm, ValidationOptions};
use serde_json::Value as JsonValue;

pub fn user_id_from_token(token: &str) -> Option<String> {
    let public_key = include_str!("../ethan-playground.pem").to_string();
    let (_header, payload) = match decode(
        &token.to_string(),
        &public_key,
        Algorithm::RS256,
        &ValidationOptions::default(),
    ) {
        Ok(d) => d,
        Err(e) => {
            println!("Error decoding & verifying key. {:?}", e);
            return None;
        }
    };
    extract_sub_from_payload(&payload)
}

fn extract_sub_from_payload(payload: &JsonValue) -> Option<String> {
    payload
        .get("sub")
        .and_then(|id| id.as_str())
        .map(|id| id.to_string())
}

#[test]
fn test_extract_sub_from_payload() {
    // arrange
    let jwt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlEwTXdSVGsyTVRjd09EZzNRakZHUTBORE1VWTVSRU00T1RKRk1qbERORFUzUWpNMVFrWTVNQSJ9.eyJpc3MiOiJodHRwczovL2V0aGFuLXBsYXlncm91bmQuYXV0aDAuY29tLyIsInN1YiI6Imdvb2dsZS1vYXV0aDJ8MTA3NTUzNjk4NjYwOTUzMTUwMTMwIiwiYXVkIjpbImh0dHBzOi8vcGxheWdyb3VuZC5ldGhhbmZyZWkuY29tIiwiaHR0cHM6Ly9ldGhhbi1wbGF5Z3JvdW5kLmF1dGgwLmNvbS91c2VyaW5mbyJdLCJpYXQiOjE1MzAzODAyNzEsImV4cCI6MTUzMDM4NzQ3MSwiYXpwIjoiNXZZRVI0dTdaS2MzS2t0SDJaNU5hTUxXTm1NNldqdU0iLCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIn0.Mlj0vU4-0z25sTi3ruxmHVD_UovTGM0S4CRopgmAl4_u9E8gsvEFFYTV-wjCByAxHhwEfJJSAFueTc_X-4zikwVAvvkyN2KPL15fj4a3_n1A3sgD_Gb314M7yc9oIyC0_0z4KB3zVwSFbyXj62YrrZHVFUzXv1apNLyCLH9dxsrASEy6YR989CdQ9v5UkApXFYP6vVEs3JxzabbCoyfeGp5Ci9QmkswZ-uN9uvco8gb4LnMqN3V-Ieb4U1MAlnjmyL2cchxzGfCdcmJ_eIR5TartQ0Qmu4etAs7nRLd-aiY4UFehc1ObKkXB23BsyGp8gwEdXv4gokholN9NLLNggw".to_string();
    let public_key = include_str!("../ethan-playground.pem").to_string();
    let (_, payload) = decode(
        &jwt,
        &public_key,
        Algorithm::RS256,
        &ValidationOptions::default(),
    )
    .ok()
    .unwrap();

    // act
    let result = extract_sub_from_payload(&payload);

    // assert
    let user_id = "google-oauth2|107553698660953150130".to_string();
    assert_eq!(result, Some(user_id))
}
