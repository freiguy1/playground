use std::ops::Deref;
use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Request, State, Outcome};
use diesel::r2d2::{ConnectionManager, Pool, PooledConnection};
use diesel::pg::PgConnection;
use dotenv::dotenv;

pub type PgPool = Pool<ConnectionManager<PgConnection>>;

// Connection request guard type: a wrapper around an r2d2 pooled connection.
pub struct DbConn(pub PooledConnection<ConnectionManager<PgConnection>>);

/// Attempts to retrieve a single connection from the managed database pool. If
/// no pool is currently managed, fails with an `InternalServerError` status. If
/// no connections are available, fails with a `ServiceUnavailable` status.
impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        let pool = request.guard::<State<'_, PgPool>>()?;
        match pool.get() {
            Ok(conn) => Outcome::Success(DbConn(conn)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ()))
        }
    }
}

// For the convenience of using an &DbConn as an &PgConnection.
impl Deref for DbConn {
    type Target = PgConnection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct UserOptional(pub Option<String>);

impl<'a, 'r> FromRequest<'a, 'r> for UserOptional{
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<UserOptional, ()> {
        let user_id = get_user_from_request(request);

        Outcome::Success(UserOptional(user_id))
    }
}

impl Deref for UserOptional {
    type Target = Option<String>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct UserRequired(pub String);

impl<'a, 'r> FromRequest<'a, 'r> for UserRequired {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<UserRequired, ()> {
        match get_user_from_request(request) {
            Some(user_id) => Outcome::Success(UserRequired(user_id)),
            None => Outcome::Failure((Status::Unauthorized, ()))
        }
    }
}

impl Deref for UserRequired {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub struct Admin(pub String);

impl<'a, 'r> FromRequest<'a, 'r> for Admin {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Admin, ()> {
        let user_id = match get_user_from_request(request) {
            Some(user_id) => user_id,
            None => return Outcome::Forward(()) //return Outcome::Failure((Status::Unauthorized, ()))
        };
        dotenv().ok();
        let is_admin = ::std::env::var("ADMIN_USER_IDS")
            .unwrap_or("".to_string())
            .split(',')
            .map(|id| id.trim())
            .any(|id| id == user_id);
        if is_admin {
            Outcome::Success(Admin(user_id))
        } else {
            // Outcome::Failure((Status::Forbidden, ()))
            Outcome::Forward(())
        }
    }
}

impl Deref for Admin {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}


// -----------------------------
//          UTILITY
// -----------------------------
fn get_user_from_request(request: &Request<'_>) -> Option<String> {
    request.headers().get_one("Authorization")
        .and_then(|s| s.split_whitespace().nth(1))
        .and_then(|t| crate::auth::user_id_from_token(t))
}
