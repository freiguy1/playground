use rocket::Rocket;
use rocket_contrib::json::Json;

use crate::guards::Admin;

mod gutenberg;
mod mc;
mod showcase;

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![is_admin, is_not_admin];
    let rocket = rocket.mount("/api", routes);
    let rocket = showcase::init_routes(rocket);
    let rocket = mc::init_routes(rocket);
    gutenberg::init_routes(rocket)
}

#[get("/is_admin")]
fn is_admin(_user_id: Admin) -> Json<bool> {
    Json(true)
}

#[get("/is_admin", rank = 2)]
fn is_not_admin() -> Json<bool> {
    Json(false)
}
