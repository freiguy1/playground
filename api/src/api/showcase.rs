use diesel::prelude::*;
use rocket::Rocket;
use rocket_contrib::json::Json;

use crate::guards::{Admin, DbConn};
use rocket::response::status::{Accepted, NotFound};
use crate::schema::{showcase_item_images, showcase_items};

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![
        get_items,
        create_item,
        update_item,
        delete_item,
        update_item_images
    ];
    rocket.mount("/api/showcase", routes)
}

// ----------- ENDPOINTS --------------

#[get("/items")]
fn get_items(db: DbConn) -> Json<Vec<Item>> {
    let item_daos = showcase_items::table
        .load::<ItemDao>(&*db)
        .expect("Error loading showcase_items");
    let image_daos = showcase_item_images::table
        .load::<ItemImageDao>(&*db)
        .expect("Error loading showcase_item_images");
    let mut items: Vec<Item> = Vec::new();
    for item_dao in item_daos.into_iter() {
        let mut image_daos: Vec<&ItemImageDao> = image_daos
            .iter()
            .filter(|im| im.item_id == item_dao.id)
            .collect();
        image_daos.sort_by(|a, b| a.sequence.cmp(&b.sequence));
        let images = image_daos
            .iter()
            .map(|im| ItemImage {
                image_url: im.image_url.clone(),
                description: im.description.clone(),
            })
            .collect();
        let new_item = Item {
            id: item_dao.id,
            name: item_dao.name,
            summary: item_dao.summary,
            description: item_dao.description,
            banner_image_url: item_dao.banner_image_url,
            price: item_dao.price,
            quantity_available: item_dao.quantity_available,
            images: images,
        };
        items.push(new_item);
    }
    items.sort_by(|a, b| a.name.cmp(&b.name));
    Json(items)
}

#[post("/items", data = "<create_item_request>")]
fn create_item(
    db: DbConn,
    _user_id: Admin,
    create_item_request: Json<CreateUpdateItem>,
) -> Json<Item> {
    let item_dao = ::diesel::insert_into(showcase_items::table)
        .values(create_item_request.into_inner())
        .get_result(&*db)
        .expect("Couldn't insert into showcase_items");
    Json(make_item_response_from_dao(item_dao, &db))
}

#[put("/items/<item_id>", data = "<update_item_request>")]
fn update_item(
    item_id: i32,
    db: DbConn,
    _user_id: Admin,
    update_item_request: Json<CreateUpdateItem>,
) -> Json<Item> {
    let item_dao = ::diesel::update(showcase_items::table.filter(showcase_items::id.eq(item_id)))
        .set(update_item_request.into_inner())
        .get_result::<ItemDao>(&*db)
        .expect("Couldn't update showcase_items");
    Json(make_item_response_from_dao(item_dao, &db))
}

#[delete("/items/<item_id>")]
fn delete_item(
    item_id: i32,
    db: DbConn,
    _user_id: Admin,
) -> Result<Accepted<()>, NotFound<String>> {
    // let item_dao = ::diesel::update(showcase_items::table.filter(showcase_items::id.eq(item_id)))
    //     .set(update_item_request.into_inner())
    //     .get_result::<ItemDao>(&*db)
    //     .expect("Couldn't update showcase_items");
    ::diesel::delete(
        showcase_item_images::table.filter(showcase_item_images::item_id.eq(&item_id)),
    )
    .execute(&*db)
    .expect("Couldn't delete showcase_item_images");

    ::diesel::delete(showcase_items::table.filter(showcase_items::id.eq(&item_id)))
        .execute(&*db)
        .expect("Couldn't delete showcase_item");
    Ok(Accepted::<()>(None))
}

#[put("/items/<item_id>/images", data = "<update_images_request>")]
fn update_item_images(
    item_id: i32,
    update_images_request: Json<Vec<ItemImage>>,
    db: DbConn,
    _user_id: Admin,
) -> Result<Json<Vec<ItemImage>>, NotFound<()>> {
    let item: Option<ItemDao> = showcase_items::table
        .filter(showcase_items::id.eq(item_id))
        .first(&*db)
        .optional()
        .expect("Error loading showcase_items");
    if item.is_none() {
        return Err(NotFound(()));
    }

    let request_with_indexes = update_images_request
        .into_inner()
        .into_iter()
        .enumerate()
        .collect::<Vec<_>>();

    let current_images = showcase_item_images::table
        .filter(showcase_item_images::item_id.eq(item_id))
        .select((
            showcase_item_images::id,
            showcase_item_images::image_url,
            showcase_item_images::description,
            showcase_item_images::sequence,
        ))
        .load::<(i32, String, Option<String>, i32)>(&*db)
        .expect("Error loading showcase_item_images");
    let ids_to_delete = current_images
        .iter()
        .filter(|i1| {
            !request_with_indexes
                .iter()
                .any(|i2| i1.1 == i2.1.image_url && i1.2 == i2.1.description && i1.3 == i2.0 as i32)
        })
        .map(|i1| i1.0)
        .collect::<Vec<_>>();
    let images_to_add = request_with_indexes
        .iter()
        .filter(|i2| {
            !current_images
                .iter()
                .any(|i1| i1.1 == i2.1.image_url && i1.2 == i2.1.description && i1.3 == i2.0 as i32)
        })
        .map(|i1| InsertItemImageDao {
            image_url: i1.1.image_url.clone(),
            description: i1.1.description.clone(),
            item_id: item_id,
            sequence: i1.0 as i32,
        })
        .collect::<Vec<_>>();

    ::diesel::delete(
        showcase_item_images::table.filter(showcase_item_images::id.eq_any(&ids_to_delete)),
    )
    .execute(&*db)
    .expect("Couldn't delete showcase_item_images");

    ::diesel::insert_into(showcase_item_images::table)
        .values(&images_to_add)
        .execute(&*db)
        .expect("Couldn't insert into showcase_item_images");

    let response = request_with_indexes.into_iter().map(|(_, i)| i).collect();
    Ok(Json(response))
}

// DATABASE HELPERS

fn make_item_response_from_dao(item_dao: ItemDao, db: &PgConnection) -> Item {
    let mut image_daos = showcase_item_images::table
        .filter(showcase_item_images::item_id.eq(item_dao.id))
        .load::<ItemImageDao>(&*db)
        .expect("Error loading showcase_item_images");
    image_daos.sort_by(|a, b| a.sequence.cmp(&b.sequence));
    let images = image_daos
        .iter()
        .map(|im| ItemImage {
            image_url: im.image_url.clone(),
            description: im.description.clone(),
        })
        .collect();
    Item {
        id: item_dao.id,
        name: item_dao.name,
        summary: item_dao.summary,
        description: item_dao.description,
        banner_image_url: item_dao.banner_image_url,
        price: item_dao.price,
        quantity_available: item_dao.quantity_available,
        images: images,
    }
}

#[derive(Insertable)]
#[table_name = "showcase_item_images"]
struct InsertItemImageDao {
    image_url: String,
    description: Option<String>,
    item_id: i32,
    sequence: i32,
}

#[derive(Queryable)]
struct ItemImageDao {
    _id: i32,
    image_url: String,
    description: Option<String>,
    item_id: i32,
    sequence: i32,
}

#[derive(Queryable)]
struct ItemDao {
    id: i32,
    name: String,
    summary: String,
    description: String,
    banner_image_url: Option<String>,
    price: Option<::bigdecimal::BigDecimal>,
    quantity_available: i32,
}

// ------------ CONTRACTS --------------

#[derive(Serialize)]
struct Item {
    id: i32,
    name: String,
    summary: String,
    description: String,
    banner_image_url: Option<String>,
    price: Option<::bigdecimal::BigDecimal>,
    quantity_available: i32,
    images: Vec<ItemImage>,
}

#[derive(Deserialize, Insertable, AsChangeset)]
#[table_name = "showcase_items"]
#[changeset_options(treat_none_as_null = "true")]
struct CreateUpdateItem {
    name: String,
    summary: String,
    description: String,
    banner_image_url: Option<String>,
    price: Option<::bigdecimal::BigDecimal>,
    quantity_available: i32,
}

#[derive(Serialize, Deserialize)]
struct ItemImage {
    image_url: String,
    description: Option<String>,
}
