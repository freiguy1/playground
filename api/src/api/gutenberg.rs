use rocket_contrib::json::JsonValue;
use rocket::Rocket;

const GUTENDEX_BASE_URL: &'static str = "http://gutendex.com";

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![
        search
    ];
    rocket.mount("/api/gutenberg", routes)
}

// ----------- ENDPOINTS --------------

#[get("/search/<query_string>?<page>")]
fn search(query_string: String, page: Option<usize>) -> Result<JsonValue, reqwest::Error> {
    let page_number = page.unwrap_or(1);
    let url = format!("{}/books/?search={}&page={:?}", GUTENDEX_BASE_URL, query_string, page_number);
    println!("url: {}", url);
    let resp: JsonValue = reqwest::get(&url)?
        .json()?;
    Ok(resp)
}

