use rocket::Rocket;
use rocket_contrib::json::Json;
use rusoto_core::Region;
use rusoto_ec2::{
    DescribeInstancesRequest, Ec2, Ec2Client, StartInstancesRequest, StopInstancesRequest,
};
use std::env;
use tokio::runtime::Runtime;

static INSTANCE_ID_ENV: &'static str = "MC_INSTANCE_ID";

pub fn init_routes(rocket: Rocket) -> Rocket {
    let routes = routes![get_status, turn_on, turn_off];
    rocket.mount("/api/mc", routes)
}

// ----------- ENDPOINTS --------------

#[get("/")]
fn get_status() -> Json<bool> {
    let ec2 = Ec2Client::new(Region::UsEast2);
    let mut t_rt = tokio::runtime::Runtime::new().unwrap();
    let is_instance_running =
        is_instance_running(&ec2, &mut t_rt).expect("Unable to get instance information");
    Json(is_instance_running)
}

#[post("/on")]
fn turn_on() {
    let ec2 = Ec2Client::new(Region::UsEast2);
    let mut t_rt = tokio::runtime::Runtime::new().unwrap();
    let is_instance_running =
        is_instance_running(&ec2, &mut t_rt).expect("Unable to get instance information");
    if is_instance_running {
        panic!("Instance is already running!");
    }

    start_instance(&ec2, &mut t_rt).expect("Unable to start instance");
}

#[post("/off")]
fn turn_off() {
    let ec2 = Ec2Client::new(Region::UsEast2);
    let mut t_rt = tokio::runtime::Runtime::new().unwrap();
    let is_instance_running =
        is_instance_running(&ec2, &mut t_rt).expect("Unable to get instance information");
    if !is_instance_running {
        panic!("Instance is already stopped!");
    }

    stop_instance(&ec2, &mut t_rt).expect("Unable to stop instance");
}

// ----------- HELPERS ---------------

fn get_instance_id() -> String {
    env::var(INSTANCE_ID_ENV).expect("MC_INSTANCE_ID must be set")
}

fn is_instance_running(ec2: &Ec2Client, t_rt: &mut Runtime) -> Result<bool, &'static str> {
    let instance_id = get_instance_id();
    let mut req = DescribeInstancesRequest::default();
    req.instance_ids = Some(vec![instance_id]);
    let state = match t_rt.block_on(ec2.describe_instances(req)) {
        Ok(result) => {
            let state = result
                .reservations
                .as_ref()
                .and_then(|rs| rs.first())
                .and_then(|r| r.instances.as_ref())
                .and_then(|is| is.first())
                .and_then(|i| i.state.clone());
            match state {
                Some(s) => s,
                _ => {
                    return Err("Instance not found");
                }
            }
        }
        Err(error) => {
            println!("{:?}", error);
            return Err("Unable to describe instances");
        }
    };
    Ok(state.code == Some(16))
}

fn start_instance(ec2: &Ec2Client, t_rt: &mut Runtime) -> Result<(), String> {
    let instance_id = get_instance_id();
    let req = StartInstancesRequest {
        instance_ids: vec![instance_id],
        ..Default::default()
    };
    t_rt.block_on(ec2.start_instances(req))
        .map(|_res| ())
        .map_err(|e| format!("Could not start instance, error = {:?}", e))
}

fn stop_instance(ec2: &Ec2Client, t_rt: &mut Runtime) -> Result<(), String> {
    let instance_id = get_instance_id();
    let req = StopInstancesRequest {
        instance_ids: vec![instance_id],
        ..Default::default()
    };
    t_rt.block_on(ec2.stop_instances(req))
        .map(|_res| ())
        .map_err(|e| format!("Could not stop instance, error = {:?}", e))
}
