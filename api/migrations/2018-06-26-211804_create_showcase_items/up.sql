-- Your SQL goes here
CREATE TABLE showcase_items (
  id SERIAL PRIMARY KEY,
  name VARCHAR NOT NULL,
  summary VARCHAR NOT NULL,
  description VARCHAR NOT NULL,
  banner_image_url VARCHAR NULL,
  price NUMERIC NULL
);

INSERT INTO showcase_items
  (name, summary, description, banner_image_url, price)
  values (
    'Sample Item',
    'This item is made with fine workmanship and good techniques.',
    'The sample item is part samp and part le. It contains some I and tem. All materials were locally sourced and there was a decent amount of handwork involved. Lorem also ipsum',
    'https://lh3.googleusercontent.com/bPfFgGPA4K1_UpearJbgG4cTJJNjhHVHeyzm-XKmCAhFfm-5cZ1iLQYuIl5DgLIj37vxHFWedpwimYB_g2mb7nz2Uw_0SmV4sawph1YSPRrCPv7YJZc1EjMWkDE_xgXLEt1CvdWGGT4Bft0X30UXjIGsl6qThMxsNI-CcyXgZ1ZU_vvVwz7RkxayX_SFTe5ygH8nneXj9YEGFPW1OjCRINmLui02c3MHE98gSb31MW3zJdxnyvU13LYIAnJcwzCqg_nN_31sANINaAW0vHzLi5q3n--UKPQDZEuy0D02H2Dp9H5KCGelNZnYMpFvqL5HRQcB04igQkGw7Csx9hk5YdxtN8-v8-1puilcib34PTjT0up7GWVwXvn2iGOdwnvcGpusZqnElk0Eb9aAm-PzqgEChx0p1Oo8Ox0M0DzUrV_xOAju_Irj6KqwldhWbrLnzGaCu8cCO7ovQmddQrHYx7PGUsp2zZFyyvOMnOchPN0J2glyzeZybxWEKKsKebHU2rNRFjMqT31gIVI9D5JKT0tfVnMJMoMpUXxfJmA9rPB2WTJLjFwRF-NwtSoxT4e2tFZjEDNQLWWFnSYdRGsN1-VQcf0NAkPsXPOOx5k1R9shCdFBrb-wHj5YvjPXEsyxOTMxbc2gX3p2SAp2nUwmdX2LyqPjeyYubw=w1920',
    45.99
  );

INSERT INTO showcase_items
  (name, summary, description, banner_image_url, price)
  values (
    'Sample Item (empty)',
    'Short reuquired summary',
    'Short required description',
    NULL,
    NULL
  );