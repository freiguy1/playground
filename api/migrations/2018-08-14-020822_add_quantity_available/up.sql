ALTER TABLE showcase_items ADD COLUMN quantity_available INTEGER;

UPDATE showcase_items set quantity_available = 0;

ALTER TABLE showcase_items
ALTER COLUMN quantity_available SET NOT NULL;