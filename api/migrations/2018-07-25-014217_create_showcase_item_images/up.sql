CREATE TABLE showcase_item_images (
  id SERIAL PRIMARY KEY,
  image_url VARCHAR NOT NULL,
  description VARCHAR NULL,
  item_id INTEGER NOT NULL REFERENCES showcase_items(id),
  sequence INTEGER NOT NULL
);
